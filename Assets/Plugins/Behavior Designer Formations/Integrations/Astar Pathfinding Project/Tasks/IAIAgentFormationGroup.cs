﻿using UnityEngine;
using BehaviorDesigner.Runtime.Formations.Tasks;
using Pathfinding;

namespace BehaviorDesigner.Runtime.Formations.AstarPathfindingProject.Tasks
{
    /// <summary>
    /// Extends FormationGroup to allow support for the IAStarAI interface.
    /// </summary>
    public class IAIAgentFormationGroup : FormationGroup
    {
        [Tooltip("The radius of the agent.")]
        public float agentRadius = 1;
        [Tooltip("The distance that the agent can stop in front of the destination.")]
        public float stoppingDistance = 0.2f;
        [Tooltip("The rotation speed of the agent.")]
        public float rotationSpeed = 100;

        /// <summary>
        /// The IAStarAIFormationAgent class contains component references and variables for each IAStarAI agent.
        /// </summary>
        public class IAStarAIFormationAgent : FormationAgent
        {
            protected float agentRadius;
            protected float stoppingDistance;
            protected float rotationSpeed;
            protected IAstarAI aStarAgent;

            public override float Speed { set { aStarAgent.maxSpeed = value; } }
            public override float Radius { get { return agentRadius; } }
            public override float RemainingDistance { get { return Vector3.Distance(transform.position, aStarAgent.destination); } }
            public override float StoppingDistance { get { return stoppingDistance; } }
            public override bool HasPath { get { return aStarAgent.hasPath; } }
            public override bool PathPending { get { return aStarAgent.pathPending; } }
            public override bool AutoBreaking { set { ; } }

            /// <summary>
            /// Caches the component references and initialize default values.
            /// </summary>
            public IAStarAIFormationAgent(Transform agent, float radius, float dist, float rotation) : base(agent)
            {
                aStarAgent = agent.GetComponent<IAstarAI>();
                agentRadius = radius;
                stoppingDistance = dist;
                rotationSpeed = rotation;

                Stop();
            }

            /// <summary>
            /// Resumes pathfinding.
            /// </summary>
            public override void Resume()
            {
                aStarAgent.canMove = true;
                aStarAgent.isStopped = false;
            }

            /// <summary>
            /// Sets the destination.
            /// </summary>
            public override void SetDestination(Vector3 destination)
            {
                if (aStarAgent.destination != destination) {
                    aStarAgent.destination = destination;
                    aStarAgent.canMove = true;
                    aStarAgent.isStopped = false;
                    aStarAgent.SearchPath();
                }
            }

            /// <summary>
            /// Rotates towards the target rotation.
            /// </summary>
            public override bool RotateTowards(Quaternion targetRotation)
            {
                if (Quaternion.Angle(transform.rotation, targetRotation) < 0.5f) {
                    return true;
                }
                transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
                return false;
            }

            /// <summary>
            /// Stops the agent from moving.
            /// </summary>
            public override void Stop()
            {
                aStarAgent.destination = transform.position;
                aStarAgent.canMove = false;
            }

            /// <summary>
            /// The task has ended. Perform any cleanup.
            /// </summary>
            public override void End()
            {
                Stop();
            }
        }

        public override void OnAwake()
        {
            base.OnAwake();

            formationAgent = new IAStarAIFormationAgent(transform, agentRadius, stoppingDistance, rotationSpeed);
        }

        public override void OnStart()
        {
            base.OnStart();

            if (leader.Value != null && leaderTree != null) {
                leaderAgent = new IAStarAIFormationAgent(leaderTree.transform, agentRadius, stoppingDistance, rotationSpeed);
            }
        }

        protected override void AddAgentToGroup(Behavior agent, int index)
        {
            base.AddAgentToGroup(agent, index);

            if (leader.Value == null) {
                formationAgents.Insert(index, new IAStarAIFormationAgent(agent.transform, agentRadius, stoppingDistance, rotationSpeed));
            }
        }
    }
}