using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Events;


namespace Animation {
    public class FarmAnimator : MonoBehaviour
    {
        [SerializeField] LooseResourceEventChannel looseResourceEventChannel;
        [SerializeField] SpatialEvent onHarvest;
        [SerializeField] SpatialEvent onGrow;
        [SerializeField] Transform babyWheat;
        [SerializeField] Transform grownWheat;
        [SerializeField] IntVariable farmYield;

        private void OnEnable() {
            babyWheat.gameObject.SetActive(false);
            grownWheat.gameObject.SetActive(false);
        }

        public void SetPhase(int phase) {
            StartCoroutine(SetPhaseCoroutine(phase));
        }

        IEnumerator SetPhaseCoroutine(int phase) {
            switch (phase) {
                case 0:
                    if (grownWheat.gameObject.activeSelf)
                        yield return ShakeAdultWheatCoroutine();
                    onHarvest?.Invoke(transform.position);
                    looseResourceEventChannel.Raise(transform.position, "Wheat", farmYield.Value);
                    grownWheat.gameObject.SetActive(false);
                    break;
                case 1:
                    onGrow?.Invoke(transform.position);
                    yield return GrowBabyWheatCoroutine();
                    break;
                case 2:
                    yield return ShakeBabyWheatCoroutine();
                    onGrow?.Invoke(transform.position);
                    yield return GrowAdultWheatCoroutine();
                    break;
            }
        }

        public IEnumerator HarvestAnimationCoroutine() {
            yield return ShakeAdultWheatCoroutine();
            grownWheat.gameObject.SetActive(false);
        }

        IEnumerator GrowBabyWheatCoroutine() {
            babyWheat.gameObject.SetActive(true);
            foreach (Transform child in babyWheat)
                child.localScale = Vector3.zero;
            int[] order = new int[babyWheat.childCount];
            for (int i = 0; i < order.Length; i++)
                order[i] = i;
            order = order.OrderBy(x => Random.Range(0f, 1f)).ToArray();
            for (int i = 0; i < order.Length; i++) {
                StartCoroutine(GrowChild(babyWheat.GetChild(order[i])));
                yield return new WaitForSeconds(.1f);
            }
        }

        IEnumerator GrowAdultWheatCoroutine() {
            babyWheat.gameObject.SetActive(false);
            grownWheat.gameObject.SetActive(true);
            foreach (Transform child in grownWheat)
                child.localScale = Vector3.zero;
            int[] order = new int[grownWheat.childCount];
            for (int i = 0; i < order.Length; i++)
                order[i] = i;
            order = order.OrderBy(x => Random.Range(0f, 1f)).ToArray();
            for (int i = 0; i < order.Length; i++) {
                StartCoroutine(GrowChild(grownWheat.GetChild(order[i])));
                yield return new WaitForSeconds(.1f);
            }
        }

        IEnumerator ShakeBabyWheatCoroutine() {
            float t = 0;
            Vector3[] startPos = new Vector3[babyWheat.childCount];
            for (int i = 0; i < babyWheat.childCount; i++)
                startPos[i] = babyWheat.GetChild(i).localPosition;
            while (t < 1) {
                t += Time.deltaTime;
                for (int i = 0; i < babyWheat.childCount; i++)
                    babyWheat.GetChild(i).localPosition = startPos[i] + Random.insideUnitSphere * Mathf.Lerp(0, .0005f, t);
                yield return null;
            }
            for (int i = 0; i < babyWheat.childCount; i++)
                babyWheat.GetChild(i).localPosition = startPos[i];
        }

        IEnumerator ShakeAdultWheatCoroutine() {
            float t = 0;
            Vector3[] startPos = new Vector3[grownWheat.childCount];
            for (int i = 0; i < grownWheat.childCount; i++)
                startPos[i] = grownWheat.GetChild(i).localPosition;
            while (t < 1) {
                t += Time.deltaTime;
                for (int i = 0; i < grownWheat.childCount; i++)
                    grownWheat.GetChild(i).localPosition = startPos[i] + Random.insideUnitSphere * Mathf.Lerp(0, .0005f, t);
                yield return null;
            }
            for (int i = 0; i < grownWheat.childCount; i++)
                grownWheat.GetChild(i).localPosition = startPos[i];
        }

        IEnumerator GrowChild(Transform child) {
            float t = 0;
            while (t < 1f) {
                t += Time.deltaTime;
                float v = Easing.Elastic(t);
                child.localScale = new Vector3(1f, 1f, v);
                yield return null;
            }
            child.localScale = Vector3.one;
        }
    }
}