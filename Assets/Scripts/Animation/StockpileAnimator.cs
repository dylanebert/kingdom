using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Logistics;

namespace Animation {
    [RequireComponent(typeof(StockpileInstance))]
    public class StockpileAnimator : MonoBehaviour
    {
        [SerializeField] GameObject[] models;

        StockpileInstance stockpileInstance;

        int count;

        private void Awake() {
            stockpileInstance = GetComponent<StockpileInstance>();
        }

        private void Update() {
            if(count != stockpileInstance.stockpile.Count) {
                count = stockpileInstance.stockpile.Count;
                UpdateVisuals();
            }
        }

        void UpdateVisuals() {
            for (int i = 0; i < models.Length; i++)
                models[i].SetActive(i < count);
        }
    }
}