using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jobs;
using Structures;

namespace Animation {
    public class CitizenAnimator : MonoBehaviour
    {
        public ParticleSystem footstep;
        public Transform root;
        public Transform body;
        public Transform head;

        Transform tr;
        Vector3 prevPos;
        float velocity;
        float t;
        float v;
        bool playedFootstep;

        private void Awake() {
            tr = transform;
            SetColors();
        }

        void SetColors() {
            int skinIdx = Random.Range(0, 8);
            MaterialPropertyBlock skinPropertyBlock = new MaterialPropertyBlock();
            skinPropertyBlock.SetFloat("Vector1_12ce682d07514486b8d91d62a0fb8fd1", skinIdx);
            head.GetComponent<MeshRenderer>().SetPropertyBlock(skinPropertyBlock);

            int shirtIdx = Random.Range(24, 44);
            MaterialPropertyBlock shirtPropertyBlock = new MaterialPropertyBlock();
            shirtPropertyBlock.SetFloat("Vector1_12ce682d07514486b8d91d62a0fb8fd1", shirtIdx);
            body.GetComponent<MeshRenderer>().SetPropertyBlock(shirtPropertyBlock);
        }

        private void Update() {
            velocity = (tr.position - prevPos).magnitude;
            if(velocity > 0 || t > .2f) {
                t += Time.deltaTime * 3;
                if (t >= 1f)
                    t -= 1f;
                v = Easing.ContinuousBounce(t);
                root.localPosition = Vector3.up * v * .3f;
                if (velocity > 0 && v < .05f && !playedFootstep) {
                    playedFootstep = true;
                    footstep.Play();
                }
                else if (v > .5f && playedFootstep) {
                    playedFootstep = false;
                }
            }
            else {
                t = 0;
                root.localPosition = Vector3.zero;
            }

            prevPos = tr.position;
        }

        public void Chop() {
            StartCoroutine(ChopCoroutine());
        }

        IEnumerator ChopCoroutine() {
            float t = 0f;
            bool chopped = false;
            while (t < 1f) {
                t += Time.deltaTime;
                float v;
                if (t < .5f) {
                    v = Easing.OutBack(t * 2);
                }
                else {
                    v = 1 - Easing.Elastic(t * 2 - 1);
                    if (v < .05f && !chopped) {
                        chopped = true;
                    }
                }
                root.localRotation = Quaternion.Slerp(Quaternion.Euler(5, -10, 0), Quaternion.Euler(-15, 45, 0), v);
                yield return null;
            }
            root.localRotation = Quaternion.identity;
        }
    }
}