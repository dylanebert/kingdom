using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Structures;

namespace Jobs
{
    public class Job
    {
        public UnityAction<Worker, Job> onExecute;
        public UnityAction<Job> onTerminate;

        public GameObject instance;
        public int maxWorkers;
        public List<Worker> workers;

        public bool Available => workers.Count < maxWorkers;

        public Job(GameObject instance, int maxWorkers) {
            this.instance = instance;
            this.maxWorkers = maxWorkers;
            workers = new List<Worker>();
        }

        public void Register(Worker worker) {
            if(!workers.Contains(worker))
                workers.Add(worker);
            worker.onExecute += Execute;
            worker.onStop += Unregister;
        }

        public void Unregister(Worker worker) {
            worker.onExecute -= Execute;
            worker.onStop -= Unregister;
            if (workers.Contains(worker))
                workers.Remove(worker);
        }

        public void Execute(Worker worker) {
            onExecute?.Invoke(worker, this);
        }

        public void Terminate() {
            onTerminate?.Invoke(this);
        }
    }
}