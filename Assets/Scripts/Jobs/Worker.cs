using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Jobs;

public class Worker
{
    public UnityAction onInterrupt;
    public UnityAction<Worker> onExecute;
    public UnityAction<Worker> onStop;

    public Job activeJob;

    public void RegisterJob(Job job) {
        if (activeJob != null)
            Debug.LogError("Registering job while worker already has job");
        activeJob = job;
        job.Register(this);
        job.onTerminate += UnregisterJob;
    }

    public void UnregisterJob(Job job) {
        job.onTerminate -= UnregisterJob;
        if (activeJob == job) {
            onInterrupt?.Invoke();
            Quit();
        }
    }

    public void Execute() {
        onExecute?.Invoke(this);
    }

    public void Quit() {
        onStop?.Invoke(this);
        activeJob = null;
    }
}
