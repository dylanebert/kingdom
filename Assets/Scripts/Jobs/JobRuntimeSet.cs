using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Jobs
{
    [CreateAssetMenu(fileName = "JobRuntimeSet", menuName = "Scriptable Objects/Jobs/Job Runtime Set")]
    public class JobRuntimeSet : ScriptableObject
    {
        public List<Job> jobs = new List<Job>();

        private void OnEnable() {
            jobs = new List<Job>();
        }

        public void Register(Job job) {
            if (!jobs.Contains(job)) 
                jobs.Add(job);
            job.onTerminate += Unregister;
        }

        public void Unregister(Job job) {
            job.onTerminate -= Unregister;
            if (jobs.Contains(job)) 
                jobs.Remove(job);
        }

        public bool Request(out Job _job) {
            _job = null;
            foreach(Job job in jobs) {
                Debug.Assert(job.instance != null);
                if(job.Available) {
                    _job = job;
                    return true;
                }
            }
            return false;
        }
    }
}