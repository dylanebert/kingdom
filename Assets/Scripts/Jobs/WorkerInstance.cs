using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime;
using Jobs;

namespace Jobs {
    [RequireComponent(typeof(Behavior))]
    public class WorkerInstance : MonoBehaviour
    {
        [HideInInspector] public Worker worker;

        Behavior behavior;

        private void Awake() {
            behavior = GetComponent<Behavior>();
            worker = new Worker();
            worker.onInterrupt += Interrupt;
        }

        public void Interrupt() {
            behavior.SendEvent("Interrupt");
        }

        private void OnEnable() {
            behavior.RegisterEvent("Work", worker.Execute);
            behavior.RegisterEvent("Quit", worker.Quit);
        }

        private void OnDisable() {
            worker.Quit();
            behavior.UnregisterEvent("Work", worker.Execute);
            behavior.UnregisterEvent("Quit", worker.Quit);
        }
    }
}