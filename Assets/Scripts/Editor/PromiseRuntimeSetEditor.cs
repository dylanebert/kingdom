using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using Logistics;

[CustomEditor(typeof(PromiseRuntimeSet))]
public class PromiseRuntimeSetEditor : Editor
{
    public override void OnInspectorGUI() {
        serializedObject.Update();
        SerializedProperty promises = serializedObject.FindProperty("promises");
        for(int i = 0; i < promises.arraySize; i++) {
            var promise = promises.GetArrayElementAtIndex(i).serializedObject.targetObject;
            EditorGUILayout.LabelField(promise.ToString());
        }
    }
}
