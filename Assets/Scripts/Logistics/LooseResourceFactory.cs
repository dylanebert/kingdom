using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Logistics 
{
    [CreateAssetMenu(fileName = "LooseResourceFactory", menuName = "Scriptable Objects/Logistics/Loose Resource Factory")]
    public class LooseResourceFactory : Factory<LooseResourceInstance>
    {
        public LooseResourceInstance prefab;

        public override LooseResourceInstance Create() {
            return Instantiate(prefab);
        }
    }
}