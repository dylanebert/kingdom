using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Logistics 
{
    public interface ISource
    {
        public UnityAction OnCollectRaised { get; set; }
        public UnityAction<ISource> OnSourceReleased { get; set; }
        public GameObject Instance { get; set; }
        public string Resource { get; }
        public int Count { get; }
        public int Capacity { get; }
        public bool HasResources { get; }
        public void RegisterPromise(Promise promise);
        public void UnregisterPromise(Promise promise);
        public void Release();
    }
}