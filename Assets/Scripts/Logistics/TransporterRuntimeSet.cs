using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Logistics 
{
    [CreateAssetMenu(fileName = "TransporterRuntimeSet", menuName = "Scriptable Objects/Logistics/Transporter Runtime Set")]
    public class TransporterRuntimeSet : ScriptableObject
    {
        public List<Transporter> transporters;

        private void OnEnable() {
            transporters = new List<Transporter>();
        }

        public void Register(Transporter transporter) {
            if(!transporters.Contains(transporter))
                transporters.Add(transporter);
            transporter.OnRelease += Unregister;
        }

        public void Unregister(Transporter transporter) {
            transporter.OnRelease -= Unregister;
            if (transporters.Contains(transporter))
                transporters.Remove(transporter);
        }
    }
}