using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Logistics 
{
    [CreateAssetMenu(fileName = "PromiseRuntimeSet", menuName = "Scriptable Objects/Logistics/Promise Runtime Set")]
    [System.Serializable]
    public class PromiseRuntimeSet : ScriptableObject
    {
        public List<Promise> promises;

        private void OnEnable() {
            promises = new List<Promise>();
        }

        public void Register(Promise promise) {
            if(!promises.Contains(promise))
                promises.Add(promise);
            promise.onRelease += Unregister;
        }

        public void Unregister(Promise promise) {
            promise.onRelease -= Unregister;
            if(promises.Contains(promise))
                promises.Remove(promise);
        }
    }
}