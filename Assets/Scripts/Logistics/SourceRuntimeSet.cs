using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Logistics 
{
    [CreateAssetMenu(fileName = "SourceRuntimeSet", menuName = "Scriptable Objects/Logistics/Source Runtime Set")]
    public class SourceRuntimeSet : ScriptableObject
    {
        public List<ISource> sources;

        private void OnEnable() {
            sources = new List<ISource>();
        }

        public void Register(ISource source) {
            sources.Add(source);
            source.OnSourceReleased += Unregister;
        }

        public void Unregister(ISource source) {
            source.OnSourceReleased -= Unregister;
            if (sources.Contains(source))
                sources.Remove(source);
        }
    }
}