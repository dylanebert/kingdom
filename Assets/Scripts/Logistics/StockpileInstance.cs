using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Events;
using Structures;

namespace Logistics
{
    public class StockpileInstance : MonoBehaviour
    {
        [SerializeField] SpatialEvent onCollect;
        [SerializeField] SpatialEvent onDeposit;
        [SerializeField] SourceRuntimeSet sourceRuntimeSet;
        [SerializeField] DestinationRuntimeSet destinationRuntimeSet;
        [SerializeField] int initialCount = 0;
        [SerializeField] int initialCapacity = 10;

        [HideInInspector] public Stockpile stockpile;

        StructureInstance structureInstance;

        void Awake() {
            structureInstance = GetComponentInParent<StructureInstance>();
            structureInstance.OnInitializeRaised += Initialize;
            structureInstance.OnDemolishRaised += Deinitialize;
        }

        public void Initialize() {
            structureInstance.OnInitializeRaised -= Initialize;
            stockpile = new Stockpile(gameObject, sourceRuntimeSet.name, initialCount, initialCapacity);
            stockpile.OnCollectRaised += OnCollect;
            stockpile.OnDepositRaised += OnDeposit;
            sourceRuntimeSet.Register(stockpile);
            destinationRuntimeSet.Register(stockpile);
        }

        public void Deinitialize() {
            structureInstance.OnDemolishRaised -= Deinitialize;
            stockpile.OnCollectRaised -= OnCollect;
            stockpile.OnDepositRaised -= OnDeposit;
            stockpile.Release();
        }

        public void OnCollect() {
            onCollect?.Invoke(transform.position);
        }

        public void OnDeposit() {
            onDeposit?.Invoke(transform.position);
        }
    }
}