using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Logistics 
{
    [CreateAssetMenu(fileName = "DestinationRuntimeSet", menuName = "Scriptable Objects/Logistics/Destination Runtime Set")]
    public class DestinationRuntimeSet : ScriptableObject
    {
        public List<IDestination> destinations;

        private void OnEnable() {
            destinations = new List<IDestination>();
        }

        public void Register(IDestination destination) {
            if(!destinations.Contains(destination))
                destinations.Add(destination);
            destination.OnDestinationReleased += Unregister;
        }

        public void Unregister(IDestination destination) {
            destination.OnDestinationReleased -= Unregister;
            if (destinations.Contains(destination))
                destinations.Remove(destination);
        }
    }
}