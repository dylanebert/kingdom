using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Events;

namespace Logistics 
{
    public class LooseResourceManager : MonoBehaviour
    {
        [SerializeField] LooseResourcePool[] looseResourcePools;
        [SerializeField] LooseResourceEventChannel looseResourceChannel;
        [SerializeField] FloatingTextEventChannel floatingTextChannel;
        [SerializeField] int initialSize = 10;

        Dictionary<string, LooseResourcePool> pools = new Dictionary<string, LooseResourcePool>();

        private void Awake() {
            for (int i = 0; i < looseResourcePools.Length; i++)
                pools.Add(looseResourcePools[i].name, looseResourcePools[i]);
        }

        private void OnEnable() {
            looseResourceChannel.OnEventRaised += CreateLooseResource;
            
            for(int i = 0; i < looseResourcePools.Length; i++) {
                looseResourcePools[i].Prewarm(initialSize);
                looseResourcePools[i].SetParent(transform);
            }
        }

        private void OnDisable() {
            looseResourceChannel.OnEventRaised -= CreateLooseResource;

            for (int i = 0; i < looseResourcePools.Length; i++)
                looseResourcePools[i].OnDisable();
        }

        public void CreateLooseResource(Vector3 pos, string resource, int count) {
            if(!pools.ContainsKey(resource)) {
                Debug.LogWarning("Resource " + resource + " missing from loose resource pools");
                return;
            }
            for (int i = 0; i < count; i++) {
                LooseResourceInstance instance = pools[resource].Request();
                instance.transform.position = pos;
                instance.Initialize();
            }
            string floatingTextString = string.Format("<sprite name=\"{0}\"> +{1}", resource, count);
            floatingTextChannel.Raise(pos, floatingTextString);
        }

    }
}