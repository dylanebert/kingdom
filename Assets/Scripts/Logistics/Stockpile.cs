using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;

namespace Logistics 
{
    [Serializable]
    public class Stockpile : ISource, IDestination
    {
        public string resource;
        public int count;
        public int capacity;

        public UnityAction OnCollectRaised { get; set; }
        public UnityAction OnDepositRaised { get; set; }
        public UnityAction<ISource> OnSourceReleased { get; set; }
        public UnityAction<IDestination> OnDestinationReleased { get; set; }
        public GameObject Instance { get; set; }
        public string Resource => resource;
        public int Count => count;
        public int Capacity => capacity;
        public bool HasResources => count - collectPromises.Count > 0;
        public bool HasSpace => count + depositPromises.Count < capacity;

        public List<Promise> collectPromises;
        public List<Promise> depositPromises;

        public Stockpile(GameObject instance, string resource, int count, int capacity) {
            Instance = instance;
            this.resource = resource;
            this.count = count;
            this.capacity = capacity;
            collectPromises = new List<Promise>();
            depositPromises = new List<Promise>();
        }

        public void RegisterPromise(Promise promise) {
            if (promise.source == this) {
                Debug.Assert(HasResources);
                if(!collectPromises.Contains(promise))
                    collectPromises.Add(promise);
                promise.onCollect += OnCollect;
            }
            else {
                Debug.Assert(HasSpace);
                if(!depositPromises.Contains(promise))
                    depositPromises.Add(promise);
                promise.onDeposit += OnDeposit;
            }
            promise.onRelease += UnregisterPromise;
        }

        public void UnregisterPromise(Promise promise) {
            promise.onRelease -= UnregisterPromise;
            promise.onCollect -= UnregisterPromise;
            promise.onDeposit -= UnregisterPromise;
            if (collectPromises.Contains(promise))
                collectPromises.Remove(promise);
            if (depositPromises.Contains(promise))
                depositPromises.Remove(promise);
        }

        public void OnCollect(Promise promise) {
            UnregisterPromise(promise);
            count--;
            OnCollectRaised?.Invoke();
        }

        public void OnDeposit(Promise promise) {
            UnregisterPromise(promise);
            count++;
            OnDepositRaised?.Invoke();
        }

        public void Release() {
            for (int i = collectPromises.Count - 1; i >= 0; i--)
                collectPromises[i].Release();
            for (int i = depositPromises.Count - 1; i >= 0; i--)
                depositPromises[i].Release();
            OnSourceReleased?.Invoke(this);
            OnDestinationReleased?.Invoke(this);
        }
    }
}