using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;

namespace Logistics 
{
    [Serializable]
    public class Destination : IDestination
    {
        public string resource;
        public int count;
        public int capacity;

        public UnityAction OnDepositRaised { get; set; }
        public UnityAction<IDestination> OnDestinationReleased { get; set; }
        public GameObject Instance { get; set; }
        public string Resource => resource;
        public int Count => count;
        public int Capacity => capacity;
        public bool HasSpace => count + promises.Count < capacity;

        List<Promise> promises;

        public Destination(GameObject instance, string resource, int count, int capacity) {
            Instance = instance;
            this.resource = resource;
            this.count = count;
            this.capacity = capacity;
            promises = new List<Promise>();
        }

        public void RegisterPromise(Promise promise) {
            Debug.Assert(HasSpace);
            if(!promises.Contains(promise))
                promises.Add(promise);
            promise.onRelease += UnregisterPromise;
            promise.onDeposit += OnDeposit;
        }

        public void UnregisterPromise(Promise promise) {
            promise.onRelease -= UnregisterPromise;
            promise.onDeposit -= UnregisterPromise;
            if(promises.Contains(promise))
                promises.Remove(promise);
        }

        public void OnDeposit(Promise promise) {
            UnregisterPromise(promise);
            count++;
            OnDepositRaised?.Invoke();
        }

        public void Release() {
            for (int i = promises.Count - 1; i >= 0; i--)
                promises[i].Release();
            OnDestinationReleased?.Invoke(this);
        }
    }
}