using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Events;
using UnityEngine.Events;

namespace Logistics 
{
    public class LooseResourceInstance : MonoBehaviour
    {
        [SerializeField] PromiseDataEventChannel channel;
        [SerializeField] SpatialEvent onCollect;
        [SerializeField] SourceRuntimeSet sourceRuntimeSet;

        Transform tr;
        Source source;
        bool initialized;

        private void Awake() {
            tr = transform;
            source = new Source(gameObject, sourceRuntimeSet.name, 1, 1);            
        }

        public void Initialize() {
            StartCoroutine(Animate());
            sourceRuntimeSet.Register(source);
            source.OnCollectRaised += OnCollect;
            initialized = true;
        }

        private void OnDisable() {
            if (!initialized) return;
            source.OnCollectRaised -= OnCollect;
            source.Release();
        }

        private void Update() {
            if (!initialized) return;
            if (source.HasResources)
                channel.Raise(source, null, null);
        }

        public void OnCollect() {
            onCollect?.Invoke(tr.position);
            Destroy(gameObject);
        }

        IEnumerator Animate() {
            float t = 0;
            float v;
            tr.position += Random.insideUnitSphere * 2;
            tr.position = new Vector3(tr.position.x, 0, tr.position.z);
            Vector3 start = tr.position + Vector3.up * 6;
            Vector3 end = tr.position;
            while (t < 1) {
                t += Time.deltaTime;
                v = Easing.Bounce(t);
                tr.position = Vector3.Lerp(start, end, v);
                yield return null;
            }
        }
    }
}