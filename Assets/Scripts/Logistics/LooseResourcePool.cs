using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Logistics 
{
    [CreateAssetMenu(fileName = "LooseResourcePool", menuName = "Scriptable Objects/Logistics/Loose Resource Pool")]
    public class LooseResourcePool : ComponentPool<LooseResourceInstance>
    {
        public LooseResourceFactory factory;

        public override IFactory<LooseResourceInstance> Factory {
            get {
                return factory;
            }
            set {
                factory = value as LooseResourceFactory;
            }
        }
    }
}