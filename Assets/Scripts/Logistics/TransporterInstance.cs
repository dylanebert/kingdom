using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using BehaviorDesigner.Runtime;
using Events;

namespace Logistics
{
    [RequireComponent(typeof(Behavior))]
    public class TransporterInstance : MonoBehaviour
    {
        [SerializeField] LooseResourceEventChannel looseResourceEventChannel;
        [SerializeField] TransporterRuntimeSet transporterRuntimeSet;
        [SerializeField] GameObject[] models;

        [HideInInspector] public Transporter transporter;

        Behavior behavior;

        private void Awake() {
            behavior = GetComponent<Behavior>();
            transporter = new Transporter(transporterRuntimeSet.name, models.Length);
        }

        private void OnEnable() {
            transporterRuntimeSet.Register(transporter);
            transporter.OnCollect += OnCollect;
            transporter.OnDeposit += OnDeposit;
            transporter.OnSeekingPromiseReleased += OnSeekingPromiseReleased;
            transporter.OnCarriedPromiseReleased += OnCarriedPromiseReleased;
            behavior.RegisterEvent<object>("Collect", Collect);
            behavior.RegisterEvent<object>("Deposit", Deposit);
        }

        private void OnDisable() {
            transporter.Release();
            transporter.OnCollect -= OnCollect;
            transporter.OnDeposit -= OnDeposit;
            transporter.OnSeekingPromiseReleased -= OnSeekingPromiseReleased;
            transporter.OnCarriedPromiseReleased -= OnCarriedPromiseReleased;
            behavior.UnregisterEvent<object>("Collect", Collect);
            behavior.UnregisterEvent<object>("Deposit", Deposit);
        }

        public void Collect(object _resource) {
            string resource = _resource as string;
            if (resource != transporterRuntimeSet.name) return;
            transporter.Collect();
        }

        public void Deposit(object _resource) {
            string resource = _resource as string;
            if (resource != transporterRuntimeSet.name) return;
            transporter.Deposit();
        }

        public void OnCollect(Transporter transporter, Promise promise) {
            if (transporter != this.transporter) return;
            for (int i = 0; i < models.Length; i++)
                models[i].SetActive(i < transporter.carrying.Count);
        }

        public void OnDeposit(Transporter transporter, Promise promise) {
            if (transporter != this.transporter) return;
            for (int i = models.Length - 1; i >= 0; i--)
                models[i].SetActive(i < transporter.carrying.Count);
        }

        public void OnSeekingPromiseReleased(Transporter transporter, Promise promise) {
            if (transporter != this.transporter) return;
            behavior.SendEvent("Interrupt");
        }

        public void OnCarriedPromiseReleased(Transporter transporter, Promise promise) {
            if (transporter != this.transporter) return;
            for (int i = models.Length - 1; i >= 0; i--)
                models[i].SetActive(i < transporter.carrying.Count);
            looseResourceEventChannel.Raise(transform.position, promise.resource, 1);
            behavior.SendEvent("Interrupt");
        }
    }
}