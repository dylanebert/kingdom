using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Events;

namespace Logistics
{
    [RequireComponent(typeof(CitizenInstance), typeof(TransporterInstance))]
    public class CitizenConsumerInstance : MonoBehaviour
    {
        [SerializeField] FloatVariable hungryTime;
        [SerializeField] Icon hungryIcon;
        [SerializeField] SpatialEvent onEat;
        [SerializeField] PromiseDataEventChannel channel;
        [SerializeField] string resource;
        [SerializeField] int initialCount = 0;
        [SerializeField] int initialCapacity = 1;

        CitizenInstance citizenInstance;
        Transporter transporter;
        Destination consumer;
        float hunger;
        bool initialized;

        private void Awake() {
            citizenInstance = GetComponent<CitizenInstance>();            
            citizenInstance.OnInitialized += Initialize;            
        }

        public void Initialize() {
            citizenInstance.OnInitialized -= Initialize;
            consumer = new Destination(gameObject, resource, initialCount, initialCapacity);
            consumer.OnDepositRaised += OnEat;
            transporter = GetComponent<TransporterInstance>().transporter;
            initialized = true;
        }

        public void OnEat() {
            onEat?.Invoke(transform.position);
        }

        private void OnDisable() {
            if (!initialized) return;
            consumer.OnDepositRaised -= OnEat;
            consumer.Release();
        }

        private void Update() {
            if (!initialized) return;
            hunger = Mathf.Clamp01(hunger - Time.deltaTime / hungryTime.Value);
            if (hunger == 0) {
                if (consumer.HasSpace) {
                    hungryIcon.Show();
                    channel.Raise(null, consumer, transporter);
                }
                else if (consumer.Count > 0) {
                    hungryIcon.Hide();
                    consumer.count--;
                    hunger += .5f;
                }
            }
        }
    }
}