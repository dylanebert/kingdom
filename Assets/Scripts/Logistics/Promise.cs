using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime;
using UnityEngine.Events;
using Events;
using System;

namespace Logistics 
{    
    [Serializable]
    public class Promise
    {
        public UnityAction<Promise> onCollect;
        public UnityAction<Promise> onDeposit;
        public UnityAction<Promise> onRelease;

        public ISource source;
        public IDestination destination;
        public Transporter transporter;
        public string resource;

        public Promise(ISource source) {
            this.source = source;
            resource = source.Resource;
        }

        public Promise(IDestination destination) {
            this.destination = destination;
            resource = destination.Resource;
        }

        public Promise(ISource source, IDestination destination) {
            this.source = source;
            this.destination = destination;
            resource = source.Resource;
        }

        public void Release() {
            onRelease?.Invoke(this);
        }

        public override string ToString() {
            return string.Format("{0} -> {1}; {2}", source, destination, transporter);
        }
    }
}