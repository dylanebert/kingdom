using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Logistics 
{
    public class Transporter
    {
        public string resource;
        public int maxCount;
        public List<Promise> promises;
        public Promise currentCollectTarget;
        public List<Promise> carrying;

        public UnityAction<Transporter, Promise> OnCollect;
        public UnityAction<Transporter, Promise> OnDeposit;
        public UnityAction<Transporter, Promise> OnSeekingPromiseReleased;
        public UnityAction<Transporter, Promise> OnCarriedPromiseReleased;
        public UnityAction<Transporter> OnRelease;

        public Transporter(string resource, int maxCount) {
            this.resource = resource;
            this.maxCount = maxCount;
            promises = new List<Promise>();
            carrying = new List<Promise>();
        }

        public bool TryRegisterPromise(Promise promise) {
            if (carrying.Count > 0)
                return false;
            if(promises.Count < maxCount) {
                if(promises.Count > 0) {
                    if(promises[0].destination == promise.destination) {
                        RegisterPromise(promise);
                        return true;
                    }
                } else {
                    RegisterPromise(promise);
                    return true;
                }
            }
            return false;
        }

        public bool GetSeekTarget() {
            if (currentCollectTarget != null) return true;
            for(int i = 0; i < promises.Count; i++) {
                if (!carrying.Contains(promises[i])) {
                    currentCollectTarget = promises[i];
                    return true;
                }
            }
            return false;
        }

        public bool ShouldDeposit() {
            if (carrying.Count == maxCount)
                return true;
            if (carrying.Count > 0 && carrying.Count == promises.Count)
                return true;
            return false;
        }

        public void RegisterPromise(Promise promise) {
            if (!promises.Contains(promise))
                promises.Add(promise);
            promise.onRelease += UnregisterPromise;
        }

        public void UnregisterPromise(Promise promise) {
            promise.onRelease -= UnregisterPromise;
            if (promises.Contains(promise))
                promises.Remove(promise);
            if (currentCollectTarget == promise) {
                currentCollectTarget = null;
                OnSeekingPromiseReleased?.Invoke(this, promise);
            }
            if (carrying.Contains(promise)) {
                carrying.Remove(promise);
                OnCarriedPromiseReleased?.Invoke(this, promise);
            }
        }

        public void Collect() {
            currentCollectTarget?.onCollect.Invoke(currentCollectTarget);            
            carrying.Add(currentCollectTarget);
            currentCollectTarget = null;
            OnCollect?.Invoke(this, currentCollectTarget);
        }

        public void Deposit() {
            currentCollectTarget = null;
            for(int i = carrying.Count - 1; i >= 0; i--) {
                Promise promise = carrying[i];
                carrying.RemoveAt(i);
                promise.onDeposit?.Invoke(promise);
                OnDeposit?.Invoke(this, promise);
                promise.Release();
            }
            Debug.Assert(promises.Count == 0);
        }

        public void Release() {
            for (int i = promises.Count - 1; i >= 0; i--)
                promises[i].Release();
            currentCollectTarget = null;
            carrying.Clear();
            OnRelease?.Invoke(this);
        }
    }

    public enum TransportState
    {
        Inactive,
        Pending,
        Collecting,
        Depositing
    }
}