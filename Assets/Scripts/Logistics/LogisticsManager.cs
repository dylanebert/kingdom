using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Events;

namespace Logistics 
{
    public class LogisticsManager : MonoBehaviour
    {
        [SerializeField] PromiseDataEventChannel channel;
        [SerializeField] PromiseRuntimeSet[] connectedPromiseRuntimeSets;
        [SerializeField] PromiseRuntimeSet[] collectPromiseRuntimeSets;
        [SerializeField] PromiseRuntimeSet[] depositPromiseRuntimeSets;
        [SerializeField] SourceRuntimeSet[] globalSourceRuntimeSets;
        [SerializeField] DestinationRuntimeSet[] globalDestinationRuntimeSets;
        [SerializeField] TransporterRuntimeSet[] transporterRuntimeSets;

        Dictionary<string, PromiseRuntimeSet> connectedPromises = new Dictionary<string, PromiseRuntimeSet>();
        Dictionary<string, PromiseRuntimeSet> collectPromises = new Dictionary<string, PromiseRuntimeSet>();
        Dictionary<string, PromiseRuntimeSet> depositPromises = new Dictionary<string, PromiseRuntimeSet>();
        Dictionary<string, SourceRuntimeSet> globalSources = new Dictionary<string, SourceRuntimeSet>();
        Dictionary<string, DestinationRuntimeSet> globalDestinations = new Dictionary<string, DestinationRuntimeSet>();
        Dictionary<string, TransporterRuntimeSet> transporters = new Dictionary<string, TransporterRuntimeSet>();

        bool succeeded;

        private void Awake() {
            for (int i = 0; i < connectedPromiseRuntimeSets.Length; i++)
                connectedPromises.Add(connectedPromiseRuntimeSets[i].name, connectedPromiseRuntimeSets[i]);
            for (int i = 0; i < collectPromiseRuntimeSets.Length; i++)
                collectPromises.Add(collectPromiseRuntimeSets[i].name, collectPromiseRuntimeSets[i]);
            for (int i = 0; i < depositPromiseRuntimeSets.Length; i++)
                depositPromises.Add(depositPromiseRuntimeSets[i].name, depositPromiseRuntimeSets[i]);
            for (int i = 0; i < globalSourceRuntimeSets.Length; i++)
                globalSources.Add(globalSourceRuntimeSets[i].name, globalSourceRuntimeSets[i]);
            for (int i = 0; i < globalDestinationRuntimeSets.Length; i++)
                globalDestinations.Add(globalDestinationRuntimeSets[i].name, globalDestinationRuntimeSets[i]);
            for (int i = 0; i < transporterRuntimeSets.Length; i++)
                transporters.Add(transporterRuntimeSets[i].name, transporterRuntimeSets[i]);
        }

        private void OnEnable() {
            channel.OnEventRaised += CreatePromise;
        }

        private void OnDisable() {
            channel.OnEventRaised -= CreatePromise;
        }

        private void Update() {
            foreach(string key in connectedPromises.Keys)
                if(connectedPromises[key].promises.Count > 0)
                    TryTransportConnectedPromise(connectedPromises[key].promises[0]);
            foreach (string key in collectPromises.Keys)
                if (collectPromises[key].promises.Count > 0)
                    TryConnectCollectPromise(collectPromises[key].promises[0]);
            foreach (string key in depositPromises.Keys)
                if (depositPromises[key].promises.Count > 0)
                    TryConnectDepositPromise(depositPromises[key].promises[0]);
        }

        void TryTransportConnectedPromise(Promise promise) {
            connectedPromises[promise.resource].promises.RemoveAt(0);
            succeeded = false;
            if (promise.transporter == null) {
                for (int i = 0; i < transporters[promise.resource].transporters.Count; i++) {
                    if (transporters[promise.resource].transporters[i].TryRegisterPromise(promise)) {
                        succeeded = true;
                        break;
                    }
                }
            } else {
                if (promise.transporter.TryRegisterPromise(promise))
                    succeeded = true;
            }
            if(!succeeded)
                connectedPromises[promise.resource].promises.Add(promise);
        }

        void TryConnectCollectPromise(Promise promise) {
            collectPromises[promise.resource].promises.RemoveAt(0);
            succeeded = false;
            for(int i = 0; i < globalDestinations[promise.resource].destinations.Count; i++) {
                if(globalDestinations[promise.resource].destinations[i].HasSpace) {
                    promise.Release();
                    channel.Raise(promise.source, globalDestinations[promise.resource].destinations[i], promise.transporter);
                    succeeded = true;
                    break;
                }
            }
            if(!succeeded)
                collectPromises[promise.resource].promises.Add(promise);
        }

        void TryConnectDepositPromise(Promise promise) {
            depositPromises[promise.resource].promises.RemoveAt(0);
            succeeded = false;
            for (int i = 0; i < globalSources[promise.resource].sources.Count; i++) {
                if (globalSources[promise.resource].sources[i].HasResources) {
                    promise.Release();
                    channel.Raise(globalSources[promise.resource].sources[i], promise.destination, promise.transporter);
                    succeeded = true;
                    break;
                }
            }
            if(!succeeded)
                depositPromises[promise.resource].promises.Add(promise);
        }

        public void CreatePromise(ISource source, IDestination destination, Transporter transporter) {
            if(source != null && destination != null) {
                Debug.Assert(source.Resource == destination.Resource, string.Format("Source has resource {0} and destination has resource {1}", source.Resource, destination.Resource));
                Promise promise = new Promise(source, destination);
                promise.transporter = transporter;
                source.RegisterPromise(promise);
                destination.RegisterPromise(promise);
                connectedPromises[promise.resource].Register(promise);
            } else if(source != null) {
                Promise promise = new Promise(source);
                promise.transporter = transporter;
                source.RegisterPromise(promise);
                collectPromises[promise.resource].Register(promise);
            } else if(destination != null) {
                Promise promise = new Promise(destination);
                promise.transporter = transporter;
                destination.RegisterPromise(promise);
                depositPromises[promise.resource].Register(promise);
            } else {
                Debug.LogError("Tried to create promise where source and destination were both null");
            }
        }
    }
}