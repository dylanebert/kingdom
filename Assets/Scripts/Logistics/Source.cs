using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;

namespace Logistics 
{
    [Serializable]
    public class Source : ISource
    {
        public string resource;
        public int count;
        public int capacity;

        public UnityAction OnCollectRaised { get; set; }
        public UnityAction<ISource> OnSourceReleased { get; set; }
        public GameObject Instance { get; set; }
        public string Resource => resource;
        public int Count => count;
        public int Capacity => capacity;
        public bool HasResources => count - promises.Count > 0;

        List<Promise> promises;

        public Source(GameObject instance, string resource, int count, int capacity) {
            Instance = instance;
            this.resource = resource;
            this.count = count;
            this.capacity = capacity;
            promises = new List<Promise>();
        }

        public void RegisterPromise(Promise promise) {
            Debug.Assert(HasResources);
            if(!promises.Contains(promise))
                promises.Add(promise);
            promise.onCollect += OnCollect;
            promise.onRelease += UnregisterPromise;
        }

        public void UnregisterPromise(Promise promise) {
            promise.onCollect -= UnregisterPromise;
            promise.onRelease -= UnregisterPromise;
            if(promises.Contains(promise))
                promises.Remove(promise);
        }

        public void OnCollect(Promise promise) {
            UnregisterPromise(promise);
            count--;
            OnCollectRaised?.Invoke();
        }

        public void Release() {
            for (int i = promises.Count - 1; i >= 0; i--)
                promises[i].Release();
            OnSourceReleased?.Invoke(this);
        }
    }
}