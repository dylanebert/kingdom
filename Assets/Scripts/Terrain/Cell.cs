﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;
using Pathfinding;
using Structures;
using Events;
using Pointers;

[Serializable]
public class Cell
{
    public GridNode Node => AstarPath.active.data.gridGraph.GetNode(x, y) as GridNode;
    public Vector3 WorldPos => new Vector3(x * 4, 0, y * 4);

    public string generationTag;
    public Color color;
    public string terrain;    
    public bool walkable;
    public bool navigable;
    public bool isWater;
    public int x;
    public int y;

    [NonSerialized] public Structure structure;
    [NonSerialized] public bool occupied;
    [NonSerialized] public bool isPath;
    [NonSerialized] public int reserved;

    public Cell(CellData data, int x, int y) {
        this.x = x;
        this.y = y;
        Apply(data);
    }

    public void Initialize() {
        structure = null;
        occupied = false;
        isPath = false;
        reserved = 0;
    }

    public void Apply(CellData data) {
        terrain = data.name;
        color = data.color;
        walkable = data.walkable;
        navigable = data.navigable;
        isWater = data.isWater;
    }

    public override string ToString() {
        return x + ", " + y;
    }

    public uint PathfindingTag {
        get {
            if (isWater)
                return 0;
            else if (!walkable)
                return 1;
            else if (!navigable)
                return 2;
            else if (isPath)
                return 5;
            else if (occupied)
                return 3;
            else
                return 4;
        }
    }
}

[Serializable]
public struct Coords
{
    public int x;
    public int y;
}