﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terrain
{
    [CreateAssetMenu(fileName = "CellRuntimeSet", menuName = "Scriptable Objects/Terrain/CellRuntimeSet")]
    [System.Serializable]
    public class CellRuntimeSet : ScriptableObject
    {
        [System.NonSerialized] public Cell[,] cells;
        [System.NonSerialized] public bool initialized = false;

        [SerializeField] Cell[] serializable;

        private void OnEnable() {
            initialized = false;
        }

        public void Initialize(Cell[,] cells) {
            this.cells = cells;
            initialized = true;
            serializable = new Cell[cells.GetLength(0) * cells.GetLength(1)];
            for (int y = 0; y < cells.GetLength(1); y++)
                for (int x = 0; x < cells.GetLength(0); x++)
                    serializable[y * cells.GetLength(0) + x] = cells[x, y];
        }

        public void Initialize(string json) {
            JsonUtility.FromJsonOverwrite(json, this);
            int size = (int)Mathf.Sqrt(serializable.Length);
            cells = new Cell[size, size];
            int x, y;
            for (int i = 0; i < serializable.Length; i++) {
                y = Mathf.FloorToInt(i / (float)size);
                x = i % size;
                cells[x, y] = serializable[i];
                cells[x, y].Initialize();
            }
        }

        public Cell ClampedCell(int x, int y) {
            x = Mathf.Clamp(x, 0, cells.GetLength(0) - 1);
            y = Mathf.Clamp(y, 0, cells.GetLength(1) - 1);
            return cells[x, y];
        }

        public Cell WorldPosToCell(Vector3 pos) {
            int x = Mathf.RoundToInt(pos.x / 4f);
            int y = Mathf.RoundToInt(pos.z / 4f);
            return ClampedCell(x, y);
        }

        public Cell CoordsToCell(Coords coords, Cell origin, Quaternion rot) {
            Vector3 vec = new Vector3(coords.x, 0, coords.y);
            vec = rot * vec;
            int x = Mathf.RoundToInt(origin.x + vec.x);
            int y = Mathf.RoundToInt(origin.y + vec.z);
            try {
                return cells[x, y];
            } catch(System.IndexOutOfRangeException) {
                Debug.LogWarning("Tried to get out of bounds cell from coords");
                return null;
            }
        }

        public Cell[] Adjacent(Cell cell) {
            Cell[] adjacent = new Cell[4];
            if (cell.y < cells.GetLength(1) - 1)
                adjacent[0] = cells[cell.x, cell.y + 1];
            if (cell.y > 0)
                adjacent[1] = cells[cell.x, cell.y - 1];
            if (cell.x < cells.GetLength(0) - 1)
                adjacent[2] = cells[cell.x + 1, cell.y];
            if (cell.x > 0)
                adjacent[3] = cells[cell.x - 1, cell.y];
            return adjacent;
        }
    }
}