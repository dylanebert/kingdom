using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Events;
using Structures;
using Procedural;

namespace Terrain
{
    public class CellManager : MonoBehaviour
    {
        [SerializeField] VoidEventChannel generateChannel;
        [SerializeField] VoidEventChannel loadChannel;
        [SerializeField] VoidEventChannel saveChannel;
        [SerializeField] StructureManager structureManager;
        [SerializeField] ProceduralData proceduralData;
        [SerializeField] CellRuntimeSet cells;
        [SerializeField] Material terrainMaterial;
        [SerializeField] Material edgeMaterial;

        private void OnEnable() {
            generateChannel.OnEventRaised += Generate;
            loadChannel.OnEventRaised += Load;
            saveChannel.OnEventRaised += Save;
        }

        private void OnDisable() {
            generateChannel.OnEventRaised -= Generate;
            loadChannel.OnEventRaised -= Load;
            saveChannel.OnEventRaised -= Save;
        }

        public void Generate() {
            proceduralData.OnTerrainGenerated += OnTerrainGenerated;
            proceduralData.GenerateTerrain();
        }

        public void Save() {
            SaveSystem.SaveTerrain(cells);
            structureManager.Save();
        }

        public void Load() {
            SaveSystem.LoadTerrain(cells);
            proceduralData.CreateGridGraph(cells.cells);
            RenderTerrain();
            structureManager.Load();
            proceduralData.FinalizeGridGraph(cells.cells);
        }

        void OnTerrainGenerated(Cell[,] cells) {
            proceduralData.OnTerrainGenerated -= OnTerrainGenerated;
            this.cells.Initialize(cells);
            RenderTerrain();
            structureManager.Generate(proceduralData);
            proceduralData.FinalizeGridGraph(cells);
        }

        void RenderTerrain() {
            GameObject terrainMesh = TerrainRenderer.Render(cells.cells, terrainMaterial, edgeMaterial);
            terrainMesh.transform.parent = transform;
        }
    }
}