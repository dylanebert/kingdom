using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CellData", menuName = "Scriptable Objects/Terrain/Cell Data")]
public class CellData : ScriptableObject
{
    public Color color;
    public bool walkable;
    public bool navigable;
    public bool isWater;
}
