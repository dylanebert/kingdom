using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

namespace Terrain
{
    public static class TerrainRenderer
    {
        public static int CHUNK_SIZE = 32;

        public static GameObject Render(Cell[,] cells, Material terrainMaterial, Material edgeMaterial) {
            int mapSize = cells.GetLength(0) / CHUNK_SIZE;
            Mesh[,] terrainMeshes = CreateTerrainMeshes(cells, mapSize);
            Texture2D[,] terrainTextures = CreateTerrainTextures(cells, mapSize);
            Mesh[,] edgeMeshes = CreateEdgeMeshes(cells, mapSize);
            GameObject root = new GameObject("Root");
            for (int i = 0; i < mapSize; i++) {
                for (int j = 0; j < mapSize; j++) {
                    GameObject terrainObj = new GameObject();
                    terrainObj.name = i + "," + j;
                    terrainObj.transform.parent = root.transform;
                    terrainObj.AddComponent<MeshFilter>().sharedMesh = terrainMeshes[i, j];
                    terrainObj.AddComponent<MeshRenderer>().material = terrainMaterial;
                    terrainObj.GetComponent<MeshRenderer>().material.mainTexture = terrainTextures[i, j];

                    GameObject edgeObj = new GameObject();
                    edgeObj.transform.parent = terrainObj.transform;
                    edgeObj.transform.localPosition = Vector3.zero;
                    edgeObj.AddComponent<MeshFilter>().sharedMesh = edgeMeshes[i, j];
                    edgeObj.AddComponent<MeshRenderer>().material = edgeMaterial;
                }
            }
            return root;
        }

        static Mesh[,] CreateTerrainMeshes(Cell[,] cells, int mapSize) {
            Mesh[,] terrainMesh = new Mesh[mapSize, mapSize];
            for (int i = 0; i < mapSize; i++) {
                for (int j = 0; j < mapSize; j++) {
                    Mesh mesh = new Mesh();

                    List<Vector3> vertices = new List<Vector3>();
                    List<int> triangles = new List<int>();
                    List<Vector2> uvs = new List<Vector2>();

                    for (int y_ = 0; y_ < CHUNK_SIZE; y_++) {
                        for (int x_ = 0; x_ < CHUNK_SIZE; x_++) {
                            int x = i * CHUNK_SIZE + x_;
                            int y = j * CHUNK_SIZE + y_;
                            if (!cells[x, y].isWater) {
                                Vector3 a = new Vector3(x * 4 - 2, 0, y * 4 + 2);
                                Vector3 b = new Vector3(x * 4 + 2, 0, y * 4 + 2);
                                Vector3 c = new Vector3(x * 4 - 2, 0, y * 4 - 2);
                                Vector3 d = new Vector3(x * 4 + 2, 0, y * 4 - 2);
                                Vector2 uvA = new Vector2(x_ / (float)CHUNK_SIZE, y_ / (float)CHUNK_SIZE);
                                Vector2 uvB = new Vector2((x_ + 1) / (float)CHUNK_SIZE, y_ / (float)CHUNK_SIZE);
                                Vector2 uvC = new Vector2(x_ / (float)CHUNK_SIZE, (y_ + 1) / (float)CHUNK_SIZE);
                                Vector2 uvD = new Vector2((x_ + 1) / (float)CHUNK_SIZE, (y_ + 1) / (float)CHUNK_SIZE);
                                Vector3[] v = new Vector3[] { a, b, c, b, d, c };
                                Vector2[] uv = new Vector2[] { uvA, uvB, uvC, uvB, uvD, uvC };
                                for (int k = 0; k < 6; k++) {
                                    vertices.Add(v[k]);
                                    uvs.Add(uv[k]);
                                    triangles.Add(triangles.Count);
                                }
                            }
                        }
                    }

                    mesh.vertices = vertices.ToArray();
                    mesh.uv = uvs.ToArray();
                    mesh.triangles = triangles.ToArray();
                    mesh.RecalculateNormals();

                    terrainMesh[i, j] = mesh;
                }
            }

            return terrainMesh;
        }

        static Texture2D[,] CreateTerrainTextures(Cell[,] cells, int mapSize) {
            Texture2D[,] textures = new Texture2D[mapSize, mapSize];

            for (int i = 0; i < mapSize; i++) {
                for (int j = 0; j < mapSize; j++) {
                    Texture2D texture = new Texture2D(CHUNK_SIZE, CHUNK_SIZE);
                    Color[] colorMap = new Color[CHUNK_SIZE * CHUNK_SIZE];
                    for (int y_ = 0; y_ < CHUNK_SIZE; y_++) {
                        for (int x_ = 0; x_ < CHUNK_SIZE; x_++) {
                            int x = i * CHUNK_SIZE + x_;
                            int y = j * CHUNK_SIZE + y_;
                            colorMap[y_ * CHUNK_SIZE + x_] = cells[x, y].color;
                        }
                    }

                    texture.filterMode = FilterMode.Point;
                    texture.wrapMode = TextureWrapMode.Clamp;
                    texture.SetPixels(colorMap);
                    texture.Apply();

                    textures[i, j] = texture;
                }
            }

            return textures;
        }

        static Mesh[,] CreateEdgeMeshes(Cell[,] cells, int mapSize) {
            Mesh[,] meshes = new Mesh[mapSize, mapSize];
            for (int i = 0; i < mapSize; i++) {
                for (int j = 0; j < mapSize; j++) {
                    Mesh mesh = new Mesh();

                    List<Vector3> vertices = new List<Vector3>();
                    List<int> triangles = new List<int>();
                    List<Vector3> normals = new List<Vector3>();

                    for (int y_ = 0; y_ < CHUNK_SIZE; y_++) {
                        for (int x_ = 0; x_ < CHUNK_SIZE; x_++) {
                            int x = i * CHUNK_SIZE + x_;
                            int y = j * CHUNK_SIZE + y_;
                            //west side
                            if (x > 0) {
                                if (cells[x - 1, y].isWater && !cells[x, y].isWater) {
                                    Vector3 a = new Vector3(x * 4 - 2, 0, y * 4 + 2);
                                    Vector3 b = new Vector3(x * 4 - 2, 0, y * 4 - 2);
                                    Vector3 c = new Vector3(x * 4 - 2, -10, y * 4 + 2);
                                    Vector3 d = new Vector3(x * 4 - 2, -10, y * 4 - 2);
                                    Vector3[] v = new Vector3[] { a, b, c, b, d, c };
                                    for (int k = 0; k < 6; k++) {
                                        vertices.Add(v[k]);
                                        triangles.Add(triangles.Count);
                                        normals.Add(Vector3.left);
                                    }
                                }
                            }
                            //east side
                            if (x < CHUNK_SIZE * mapSize - 1) {
                                if (cells[x + 1, y].isWater && !cells[x, y].isWater) {
                                    Vector3 a = new Vector3(x * 4 + 2, 0, y * 4 - 2);
                                    Vector3 b = new Vector3(x * 4 + 2, 0, y * 4 + 2);
                                    Vector3 c = new Vector3(x * 4 + 2, -10, y * 4 - 2);
                                    Vector3 d = new Vector3(x * 4 + 2, -10, y * 4 + 2);
                                    Vector3[] v = new Vector3[] { a, b, c, b, d, c };
                                    for (int k = 0; k < 6; k++) {
                                        vertices.Add(v[k]);
                                        triangles.Add(triangles.Count);
                                        normals.Add(Vector3.right);
                                    }
                                }
                            }
                            //south side
                            if (y > 0) {
                                if (cells[x, y - 1].isWater && !cells[x, y].isWater) {
                                    Vector3 a = new Vector3(x * 4 - 2, 0, y * 4 - 2);
                                    Vector3 b = new Vector3(x * 4 + 2, 0, y * 4 - 2);
                                    Vector3 c = new Vector3(x * 4 - 2, -10, y * 4 - 2);
                                    Vector3 d = new Vector3(x * 4 + 2, -10, y * 4 - 2);
                                    Vector3[] v = new Vector3[] { a, b, c, b, d, c };
                                    for (int k = 0; k < 6; k++) {
                                        vertices.Add(v[k]);
                                        triangles.Add(triangles.Count);
                                        normals.Add(Vector3.forward);
                                    }
                                }
                            }
                            //north side
                            if (y < CHUNK_SIZE * mapSize - 1) {
                                if (cells[x, y + 1].isWater && !cells[x, y].isWater) {
                                    Vector3 a = new Vector3(x * 4 + 2, 0, y * 4 + 2);
                                    Vector3 b = new Vector3(x * 4 - 2, 0, y * 4 + 2);
                                    Vector3 c = new Vector3(x * 4 + 2, -10, y * 4 + 2);
                                    Vector3 d = new Vector3(x * 4 - 2, -10, y * 4 + 2);
                                    Vector3[] v = new Vector3[] { a, b, c, b, d, c };
                                    for (int k = 0; k < 6; k++) {
                                        vertices.Add(v[k]);
                                        triangles.Add(triangles.Count);
                                        normals.Add(Vector3.back);
                                    }
                                }
                            }
                        }
                    }

                    mesh.vertices = vertices.ToArray();
                    mesh.triangles = triangles.ToArray();
                    mesh.normals = normals.ToArray();

                    meshes[i, j] = mesh;
                }
            }

            return meshes;
        }
    }
}