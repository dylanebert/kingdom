using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Events;

namespace UI 
{
    public class PointerWindow : MonoBehaviour
    {
        [SerializeField] GameObject contents;
        [SerializeField] TextMeshProUGUI text;
        [SerializeField] StringEventChannel showPointerWindowChannel;
        [SerializeField] VoidEventChannel hidePointerWindowChannel;

        Canvas canvas;
        RectTransform canvasRect;

        private void Awake() {
            canvas = GetComponentInParent<Canvas>();
            canvasRect = (RectTransform)canvas.transform;
        }

        private void OnEnable() {
            showPointerWindowChannel.OnEventRaised += Show;
            hidePointerWindowChannel.OnEventRaised += Hide;
        }

        private void OnDisable() {
            showPointerWindowChannel.OnEventRaised -= Show;
            hidePointerWindowChannel.OnEventRaised -= Hide;
        }

        private void Update() {
            if (!contents.activeSelf)
                return;
            Vector2 anchorPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y) - new Vector2(canvasRect.position.x, canvasRect.position.y);
            anchorPos = new Vector2(anchorPos.x / canvasRect.lossyScale.x, anchorPos.y / canvasRect.lossyScale.y);
            ((RectTransform)transform).anchoredPosition = anchorPos;
        }

        public void Show(string text) {
            contents.SetActive(true);
            this.text.text = text;
        }

        public void Hide() {
            contents.SetActive(false);
        }
    }
}