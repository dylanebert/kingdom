using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Structures;
using Events;

namespace UI
{
    public class BlueprintButton : MonoBehaviour
    {
        [SerializeField] Button button;
        [SerializeField] TextMeshProUGUI title;
        [SerializeField] BlueprintDataEventChannel blueprintCreationChannel;

        BlueprintData data;

        public void Initialize(BlueprintData data) {
            this.data = data;
            title.text = data.name;
            button.onClick.AddListener(OnClick);
        }

        public void OnClick() {
            blueprintCreationChannel.Raise(data);
        }
    }
}