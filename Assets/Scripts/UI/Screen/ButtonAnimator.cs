using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Events;

namespace UI
{
    public class ButtonAnimator : MonoBehaviour
    {
        [SerializeField] ButtonColorData colorData;
        [SerializeField] ButtonEventChannel eventChannel;
        [SerializeField] Image background;
        [SerializeField] Image shockwave;
        [SerializeField] Image icon;

        ButtonState state;
        float t, v;

        private void OnEnable() {
            eventChannel.OnEventRaised += SetState;
        }

        private void OnDisable() {
            eventChannel.OnEventRaised -= SetState;
        }

        void Update() {
            if (state == ButtonState.Active) {
                if (t < 1) {
                    shockwave.gameObject.SetActive(true);
                    t += Time.unscaledDeltaTime * 3f;
                    v = Easing.Elastic(t);
                    icon.transform.localScale = Vector3.one * v;
                    v = 1 - (1 - t) * (1 - t);                    
                    shockwave.transform.localScale = Vector3.one * (1 + v);
                    shockwave.color = new Color(255, 255, 255, 1 - v);
                }
                else {
                    shockwave.gameObject.SetActive(false);
                }
            }
            else {
                shockwave.gameObject.SetActive(false);
            }
        }

        public void SetState(ButtonAction action, ButtonState state) {
            this.state = state;
            switch(state) {
                case ButtonState.Normal:
                    background.color = colorData.normal;
                    break;
                case ButtonState.Hover:
                    background.color = colorData.hover;
                    break;
                case ButtonState.Disabled:
                    background.color = colorData.disabled;
                    break;
                case ButtonState.Active:
                    t = 0;
                    background.color = colorData.active;
                    break;
            }
        }
    }
}