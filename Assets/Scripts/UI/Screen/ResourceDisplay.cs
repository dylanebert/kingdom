using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Logistics;
using TMPro;

namespace UI
{
    public class ResourceDisplay : MonoBehaviour
    {
        [SerializeField] SourceRuntimeSet source;
        [SerializeField] Transform sprite;
        [SerializeField] TextMeshProUGUI text;

        float t, v;
        int prevCount;

        private void Update() {
            int count = 0;
            for (int i = 0; i < source.sources.Count; i++)
                count += source.sources[i].Count;

            if(count != prevCount) {
                prevCount = count;
                text.text = count.ToString();
                t = 0;
            }        

            if(t < 1) {
                t += Time.unscaledDeltaTime;
                v = Easing.Elastic(t);
                sprite.localScale = Vector3.one * v;
            }
        }
    }
}