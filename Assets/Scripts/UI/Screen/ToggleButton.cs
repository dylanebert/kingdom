using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using Events;

namespace UI
{
    public class ToggleButton : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField] ButtonEventChannel eventChannel;
        [SerializeField] VoidEventChannel toggleGroupClearChannel;
        [SerializeField] UnityEvent onClick;

        bool toggled;

        void OnEnable() {
            toggleGroupClearChannel.OnEventRaised += Clear;
        }

        void OnDisable() {
            toggleGroupClearChannel.OnEventRaised -= Clear;
        }

        void Clear() {
            if (toggled) {
                toggled = false;
                eventChannel.Raise(ButtonAction.None, ButtonState.Normal);
            }
        }

        public void OnPointerClick(PointerEventData eventData) {
            onClick?.Invoke();
            bool prev = toggled;
            toggleGroupClearChannel.Raise();
            toggled = !prev;
            if (toggled)
                eventChannel.Raise(ButtonAction.Clicked, ButtonState.Active);
            else
                eventChannel.Raise(ButtonAction.Unclicked, ButtonState.Normal);
        }

        public void OnPointerEnter(PointerEventData eventData) {
            if (toggled) return;
            eventChannel.Raise(ButtonAction.HoverEnter, ButtonState.Hover);
        }

        public void OnPointerExit(PointerEventData eventData) {
            if (toggled) return;
            eventChannel.Raise(ButtonAction.HoverExit, ButtonState.Normal);
        }
    }
}