using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UI
{
    [CreateAssetMenu(fileName = "ButtonColorData", menuName = "Scriptable Objects/UI/Button Color Data")]
    public class ButtonColorData : ScriptableObject
    {
        public Color normal;
        public Color hover;
        public Color disabled;
        public Color active;
    }
}