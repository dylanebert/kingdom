using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Events;
using UnityEngine.UI;

namespace UI 
{
    [RequireComponent(typeof(Button))]
    public class ChopCancelButton : MonoBehaviour
    {
        [SerializeField] VoidEventChannel chopCancelChannel;
        [SerializeField] VoidEventChannel showChannel;
        [SerializeField] VoidEventChannel hideChannel;

        Button button;
        bool active;
        float t, v;

        private void Awake() {
            button = GetComponent<Button>();
            transform.localScale = Vector3.zero;
        }

        private void Update() {
            if(active) {
                if(t < 1) {
                    t += Time.unscaledDeltaTime;
                    v = Easing.Elastic(t);
                    transform.localScale = Vector3.one * v;
                }
            } else {
                if(t > 0) {
                    t -= Time.unscaledDeltaTime * 2;
                    v = Easing.Elastic(t);
                    transform.localScale = Vector3.one * v;
                }
            }
        }

        private void OnEnable() {
            showChannel.OnEventRaised += Show;
            hideChannel.OnEventRaised += Hide;
            button.onClick.AddListener(OnClick);
        }

        private void OnDisable() {
            showChannel.OnEventRaised -= Show;
            hideChannel.OnEventRaised -= Hide;
            button.onClick.RemoveListener(OnClick);
        }

        public void Show() {
            active = true;
        }

        public void Hide() {
            active = false;
        }

        public void OnClick() {
            chopCancelChannel.Raise();
            hideChannel.Raise();
        }
    }
}