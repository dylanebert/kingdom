using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Events;

namespace UI
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class Title : MonoBehaviour
    {
        [SerializeField] StringEventChannel showChannel;

        TextMeshProUGUI text;
        float t;
        float lifetime;
        bool active;

        private void Awake() {
            text = GetComponent<TextMeshProUGUI>();
        }

        private void OnEnable() {
            showChannel.OnEventRaised += Show;
        }

        private void OnDisable() {
            showChannel.OnEventRaised -= Show;
        }

        private void Update() {
            if (lifetime > 0)
                lifetime -= Time.unscaledDeltaTime;
            else
                active = false;

            if(active) {
                text.color = Color.white;
                if(t < 1f) {
                    t += Time.unscaledDeltaTime * 2f;
                    float v = Easing.Elastic(t);
                    transform.localScale = new Vector3(v, 1f, 1f);
                } else {
                    transform.localScale = Vector3.one;
                }
            } else {
                if(t > 0f) {
                    t -= Time.unscaledDeltaTime * 3f;
                    float v = t * t;
                    text.color = Color.Lerp(new Color(1, 1, 1, 0), Color.white, v);
                } else {
                    text.color = new Color(1, 1, 1, 0);
                }
            }
        }

        public void Show(string text) {
            this.text.text = text;
            lifetime = 5f;
            t = 0f;
            active = true;
        }

        public void Hide() {
            active = false;
        }
    }
}