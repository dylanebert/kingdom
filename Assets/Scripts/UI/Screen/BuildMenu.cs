using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Structures;
using Events;

namespace UI
{
    public class BuildMenu : MonoBehaviour
    {
        [SerializeField] BlueprintData[] blueprints;
        [SerializeField] BlueprintButton blueprintButtonPrefab;
        [SerializeField] RectTransform contents;
        [SerializeField] ButtonEventChannel showBuildMenuChannel;
        [SerializeField] VoidEventChannel hideBuildMenuChannel;

        bool active;
        float t, v;

        private void Awake() {
            for(int i = 0; i < blueprints.Length; i++) {
                if(!blueprints[i].unlocked) continue;
                BlueprintButton button = Instantiate(blueprintButtonPrefab, contents.transform);
                button.Initialize(blueprints[i]);
            }
        }

        private void OnEnable() {
            showBuildMenuChannel.OnEventRaised += Toggle;
            hideBuildMenuChannel.OnEventRaised += Hide;
        }

        private void OnDisable() {
            showBuildMenuChannel.OnEventRaised -= Toggle;
            hideBuildMenuChannel.OnEventRaised -= Hide;
        }

        private void Update() {
            if (active) {
                contents.gameObject.SetActive(true);
                if (t < 1f) {
                    t += Time.unscaledDeltaTime * 2f;
                    v = Easing.Exponential(t);
                    contents.localScale = new Vector3(v, 1, 1);
                }
                else {
                    contents.localScale = Vector3.one;
                }
            }
            else {
                if (t > 0f) {
                    t -= Time.unscaledDeltaTime * 5f;
                    v = Easing.Exponential(t);
                    contents.localScale = new Vector3(1, v, 1);
                }
                else {
                    contents.gameObject.SetActive(false);
                }
            }
        }

        public void Toggle(ButtonAction action, ButtonState state) {
            if(state == ButtonState.Active) {
                Show();
            } else {
                Hide();
            }
        }

        public void Show() {
            active = true;
        }

        public void Hide() {
            active = false;
        }
    }
}