using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressIcon : Icon
{
    [SerializeField] Image progressImage;

    float prevProgress;
    float progress;
    float t0, v0;

    private void Awake() {
        progress = 0;
    }

    protected override void Update() {
        base.Update();
        if(t0 < 1) {
            t0 += Time.unscaledDeltaTime;
            v0 = Easing.OutBack(t0);
            progressImage.fillAmount = 1 - Mathf.Lerp(prevProgress, progress, v0);
        }
    }

    public void SetProgress(float progress) {
        if(this.progress != progress) {
            prevProgress = this.progress;
            this.progress = progress;
            t0 = 0;
        }
    }
}