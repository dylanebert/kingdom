using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Events;

public class Icon : MonoBehaviour
{
    [SerializeField] GameObject contents;

    public void OnValidate() {
        if (contents == null)
            Debug.LogWarning("Missing references on Icon " + gameObject.name);
    }

    float t, v;
    bool active;

    private void Start() {
        contents.transform.localScale = Vector3.zero;
    }

    protected virtual void Update() {
        if (active) {
            contents.SetActive(true);
            if (t < 1) {
                t += Time.unscaledDeltaTime;
                v = Easing.Elastic(t);
                contents.transform.localScale = Vector3.one * v;
            }
        }
        else {
            if (t > 0) {
                t -= Time.unscaledDeltaTime * 4;
                v = Easing.OutBack(t);
                contents.transform.localScale = Vector3.one * v;
            } else {
                contents.SetActive(false);
            }
        }
    }

    public void Show() {
        active = true;
    }

    public void Hide() {
        active = false;
    }
}