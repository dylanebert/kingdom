using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Events;
using Structures;

public class TileIndicator : MonoBehaviour
{
    [SerializeField] SpriteRenderer sprite;
    [SerializeField] Icon axeIcon;
    [SerializeField] VoidEventChannel showChannel;
    [SerializeField] VoidEventChannel hideChannel;
    [SerializeField] CellEventChannel hoverChannel;
    [SerializeField] CellDragEventChannel dragChannel;
    [SerializeField] CellDragEventChannel dragEndChannel;
    [SerializeField] StructureEventChannel structureHoverChannel;
    [SerializeField] VoidEventChannel axeIconShowChannel;
    [SerializeField] VoidEventChannel axeIconHideChannel;

    Transform tr;

    private void Awake() {
        tr = transform;
    }

    private void OnEnable() {
        showChannel.OnEventRaised += Show;
        hideChannel.OnEventRaised += Hide;
        hoverChannel.OnEventRaised += SetHover;
        dragChannel.OnEventRaised += SetDrag;
        dragEndChannel.OnEventRaised += DragEnd;
        structureHoverChannel.OnEventRaised += HoverStructure;
        axeIconShowChannel.OnEventRaised += ShowAxeIcon;
        axeIconHideChannel.OnEventRaised += HideAxeIcon;
    }

    private void OnDisable() {
        showChannel.OnEventRaised -= Show;
        hideChannel.OnEventRaised -= Hide;
        hoverChannel.OnEventRaised -= SetHover;
        dragChannel.OnEventRaised -= SetDrag;
        dragEndChannel.OnEventRaised -= DragEnd;
        structureHoverChannel.OnEventRaised -= HoverStructure;
        axeIconShowChannel.OnEventRaised -= ShowAxeIcon;
        axeIconHideChannel.OnEventRaised -= HideAxeIcon;
    }

    public void Show() {
        sprite.enabled = true;
    }

    public void Hide() {
        sprite.enabled = false;
    }

    public void ShowAxeIcon() {
        axeIcon.Show();
    }

    public void HideAxeIcon() {
        axeIcon.Hide();
    }

    public void SetHover(Cell cell) {
        tr.position = cell.WorldPos;
        sprite.size = Vector2.one * 4;
    }

    public void HoverStructure(Structure structure) {
        Cell min = structure.cells[0];
        Cell max = structure.cells[0];
        for(int i = 0; i < structure.cells.Length; i++) {
            Cell cell = structure.cells[i];
            if (cell.x < min.x || cell.y < min.y)
                min = cell;
            if (cell.x > max.x || cell.y > max.y)
                max = cell;
        }
        SetDrag(min, max);
    }

    public void SetDrag(Cell start, Cell end) {
        int xMin = Mathf.Min(start.x, end.x);
        int xMax = Mathf.Max(start.x, end.x);
        int yMin = Mathf.Min(start.y, end.y);
        int yMax = Mathf.Max(start.y, end.y);
        Vector2 size = new Vector2((xMax - xMin + 1) * 4, (yMax - yMin + 1) * 4);
        Vector3 pos = (start.WorldPos + end.WorldPos) / 2f;
        tr.position = pos;
        sprite.size = size;
    }

    public void DragEnd(Cell start, Cell end) {
        SetHover(end);
    }

    public void SetColor(Color c) {
        sprite.enabled = true;
        sprite.color = c;
    }
}
