using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FloatingTextPool", menuName = "Scriptable Objects/UI/Floating Text Pool")]
public class FloatingTextPool : ComponentPool<FloatingText>
{
    public FloatingTextFactory factory;

    public override IFactory<FloatingText> Factory { 
        get {
            return factory;
        } set {
            factory = value as FloatingTextFactory;
        }
    }
}
