using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

public class FloatingText : MonoBehaviour
{
    public UnityAction<FloatingText> OnFinishedPlaying;

    public TextMeshPro text;

    public IEnumerator Animate(bool bounce) {
        float t = 0f;
        float v;
        Vector3 startPos = transform.position = transform.position + Vector3.up * 5f + Random.insideUnitSphere * 5;
        Vector3 endPos = startPos + Vector3.up * Random.Range(3f, 5f);
        text.color = Color.white;
        if (!bounce)
            t = 1f;
        while (t < 1f) {
            t += Time.unscaledDeltaTime;
            v = Easing.Bounce(t);
            transform.position = Vector3.Lerp(startPos, endPos, v);
            yield return null;
        }
        yield return new WaitForSecondsRealtime(1);
        while(t > 0) {
            t -= Time.unscaledDeltaTime;
            v = 1 - (1 - t) * (1 - t);
            text.color = new Color(255, 255, 255, v);
            yield return null;
        }
        OnFinishedPlaying?.Invoke(this);
    }
}
