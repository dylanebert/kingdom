using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FloatingTextFactory", menuName = "Scriptable Objects/UI/Floating Text Factory")]
public class FloatingTextFactory : Factory<FloatingText>
{
    public FloatingText prefab;

    public override FloatingText Create() {
        return Instantiate(prefab);
    }
}
