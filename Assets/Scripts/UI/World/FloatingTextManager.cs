using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UI;
using UnityEngine.Events;
using Events;

public class FloatingTextManager : MonoBehaviour
{
    [SerializeField] FloatingTextEventChannel channel;
    [SerializeField] FloatingTextPool pool;
    [SerializeField] int initialSize = 10;

    private void OnEnable() {
        channel.OnEventRaised += CreateFloatingText;

        pool.Prewarm(initialSize);
        pool.SetParent(transform);
    }

    private void OnDisable() {
        channel.OnEventRaised -= CreateFloatingText;
    }

    public void CreateFloatingText(Vector3 pos, string text) {
        FloatingText floatingText = pool.Request();
        floatingText.text.text = text;
        floatingText.transform.position = pos;
        floatingText.OnFinishedPlaying += OnFloatingTextFinishedPlaying;
        StartCoroutine(floatingText.Animate(true));
    }

    public void OnFloatingTextFinishedPlaying(FloatingText floatingText) {
        floatingText.OnFinishedPlaying -= OnFloatingTextFinishedPlaying;
        pool.Return(floatingText);
    }
}
