using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Events
{
    [System.Serializable]
    public class SpatialEvent : UnityEvent<Vector3> { }

    [CreateAssetMenu(fileName = "SpatialEvent", menuName = "Scriptable Objects/Events/Spatial Event Channel")]
    public class SpatialEventChannel : ScriptableObject
    {
        public UnityAction<Vector3> OnEventRaised;

        public void Raise(Vector3 value) {
            if (OnEventRaised != null)
                OnEventRaised.Invoke(value);
        }
    }
}