using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Audio;

namespace Events
{
    [CreateAssetMenu(fileName = "AudioEventChannel", menuName = "Scriptable Objects/Audio/Audio Event Channel")]
    public class AudioEventChannel : ScriptableObject
    {
        public UnityAction<AudioCue, Vector3> OnAudioRequested;

        public void Raise(AudioCue cue, Vector3 pos = default) {
            OnAudioRequested?.Invoke(cue, pos);
        }
    }
}