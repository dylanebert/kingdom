using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Pointers;

namespace Events
{
    [System.Serializable]
    public class CellDragEvent : UnityEvent<Cell, Cell> { }

    [CreateAssetMenu(fileName = "CellDragEventChannel", menuName = "Scriptable Objects/Events/Cell Drag Event Channel")]
    public class CellDragEventChannel : ScriptableObject
    {
        public UnityAction<Cell, Cell> OnEventRaised;

        public void Raise(Cell start, Cell end) {
            if (OnEventRaised != null)
                OnEventRaised.Invoke(start, end);
        }
    }
}