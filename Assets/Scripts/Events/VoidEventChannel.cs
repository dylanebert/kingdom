using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Events
{
    [CreateAssetMenu(fileName = "VoidEvent", menuName = "Scriptable Objects/Events/Void Event Channel")]
    public class VoidEventChannel : ScriptableObject
    {
        public UnityAction OnEventRaised;

        public void Raise() {
            if (OnEventRaised != null)
                OnEventRaised.Invoke();
        }
    }
}