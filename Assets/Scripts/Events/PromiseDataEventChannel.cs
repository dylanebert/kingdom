using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Structures;
using Logistics;

namespace Events
{
    [System.Serializable]
    public class PromiseDataEvent : UnityEvent<ISource, IDestination> { }

    [CreateAssetMenu(fileName = "PromiseDataEventChannel", menuName = "Scriptable Objects/Events/Promise Data Event Channel")]
    public class PromiseDataEventChannel : ScriptableObject
    {
        public UnityAction<ISource, IDestination, Transporter> OnEventRaised;

        public void Raise(ISource source, IDestination destination, Transporter transporter) {
            OnEventRaised?.Invoke(source, destination, transporter);
        }
    }
}