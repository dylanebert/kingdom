using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Events
{
    [System.Serializable]
    public class ButtonEvent : UnityEvent<ButtonAction, ButtonState> { }

    [CreateAssetMenu(fileName = "ButtonEvent", menuName = "Scriptable Objects/Events/Button Event Channel")]
    public class ButtonEventChannel : ScriptableObject
    {
        public UnityAction<ButtonAction, ButtonState> OnEventRaised;

        public void Raise(ButtonAction action, ButtonState state) {
            if (OnEventRaised != null)
                OnEventRaised.Invoke(action, state);
        }
    }

    public enum ButtonState
    {
        Normal,
        Hover,
        Disabled,
        Active
    }

    public enum ButtonAction
    {
        None,
        Clicked,
        Unclicked,
        HoverEnter,
        HoverExit,
        Enabled,
        Disabled
    }
}