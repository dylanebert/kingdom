using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Events
{
    [System.Serializable]
    public class LooseResourceEvent : UnityEvent<Vector3, string, int> { }

    [CreateAssetMenu(fileName = "SpatialStringEvent", menuName = "Scriptable Objects/Events/Spatial String Event Channel")]
    public class LooseResourceEventChannel : ScriptableObject
    {
        public UnityAction<Vector3, string, int> OnEventRaised;

        public void Raise(Vector3 pos, string resource, int count) {
            OnEventRaised.Invoke(pos, resource, count);
        }
    }
}