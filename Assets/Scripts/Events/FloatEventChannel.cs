using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Events
{
    [System.Serializable]
    public class FloatEvent : UnityEvent<float> { }

    [CreateAssetMenu(fileName = "FloatEvent", menuName = "Scriptable Objects/Events/Float Event Channel")]
    public class FloatEventChannel : ScriptableObject
    {
        public UnityAction<float> OnEventRaised;

        public void Raise(float value) {
            if(OnEventRaised != null)
                OnEventRaised.Invoke(value);
        }
    }
}