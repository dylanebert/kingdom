using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Events
{
    [CreateAssetMenu(fileName = "TreeEvents", menuName = "Scriptable Objects/Events/Tree Events")]
    public class TreeEvents : ScriptableObject
    {
        public UnityEvent onHit;
        public UnityEvent onFall;
        public UnityEvent onHitGround;
        public UnityEvent onPop;
    }
}