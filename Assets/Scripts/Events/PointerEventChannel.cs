using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Structures;
using Pointers;

namespace Events
{
    [System.Serializable]
    public class PointerEvent : UnityEvent<Pointer> { }

    [CreateAssetMenu(fileName = "PointerEventChannel", menuName = "Scriptable Objects/Events/Pointer Event Channel")]
    public class PointerEventChannel : ScriptableObject
    {
        public UnityAction<Pointer> OnEventRaised;

        public void Raise(Pointer data) {
            if (OnEventRaised != null)
                OnEventRaised.Invoke(data);
        }
    }
}
