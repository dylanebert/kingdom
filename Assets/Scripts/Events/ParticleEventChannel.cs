using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using FX;

namespace Events
{
    [CreateAssetMenu(fileName = "ParticleEventChannel", menuName = "Scriptable Objects/FX/Particle Event Channel")]
    public class ParticleEventChannel : ScriptableObject
    {
        public UnityAction<ParticleCue, Vector3> OnParticleRequested;

        public void Raise(ParticleCue cue, Vector3 pos) {
            if (OnParticleRequested != null)
                OnParticleRequested.Invoke(cue, pos);
            else
                Debug.LogWarning("A particle cue was requested, but nobody picked it up.");
        }
    }
}