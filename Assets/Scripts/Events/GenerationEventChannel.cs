using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Events
{
    [System.Serializable]
    public class GenerationEvent : UnityEvent<string, Cell, Quaternion> { }

    [CreateAssetMenu(fileName = "GenerationEventChannel", menuName = "Scriptable Objects/Events/Generation Event Channel")]
    public class GenerationEventChannel : ScriptableObject
    {
        public UnityAction<string, Cell, Quaternion> OnEventRaised;

        public void Raise(string value, Cell pos, Quaternion rot) {
            OnEventRaised?.Invoke(value, pos, rot);
        }
    }
}