using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Structures;
using Pointers;

namespace Events
{
    [System.Serializable]
    public class StructureInstanceEvent : UnityEvent<StructureInstance, Cell, Quaternion> { }

    [CreateAssetMenu(fileName = "StructureInstanceEventChannel", menuName = "Scriptable Objects/Events/Structure Instance Event Channel")]
    public class StructureInstanceEventChannel : ScriptableObject
    {
        public UnityAction<StructureInstance, Cell, Quaternion> OnEventRaised;

        public void Raise(StructureInstance structure, Cell pos, Quaternion rot) {
            OnEventRaised?.Invoke(structure, pos, rot);
        }
    }
}