using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Pointers;

namespace Events
{
    [System.Serializable]
    public class CellEvent : UnityEvent<Cell> { }

    [CreateAssetMenu(fileName = "CellEventChannel", menuName = "Scriptable Objects/Events/Cell Event Channel")]
    public class CellEventChannel : ScriptableObject
    {
        public UnityAction<Cell> OnEventRaised;

        public void Raise(Cell cell) {
            OnEventRaised?.Invoke(cell);
        }
    }
}