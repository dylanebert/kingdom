using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Events
{
    [System.Serializable]
    public class StringEvent : UnityEvent<string> { }

    [CreateAssetMenu(fileName = "StringEvent", menuName = "Scriptable Objects/Events/String Event Channel")]
    public class StringEventChannel : ScriptableObject
    {
        public UnityAction<string> OnEventRaised;

        public void Raise(string value) {
            OnEventRaised?.Invoke(value);
        }
    }
}