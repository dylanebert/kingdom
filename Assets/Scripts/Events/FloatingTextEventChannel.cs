using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Events
{
    [System.Serializable]
    public class FloatingTextEvent : UnityEvent<Vector3, string> { }

    [CreateAssetMenu(fileName = "FloatingTextEvent", menuName = "Scriptable Objects/Events/Floating Text Event Channel")]
    public class FloatingTextEventChannel : ScriptableObject
    {
        public UnityAction<Vector3, string> OnEventRaised;

        public void Raise(Vector3 pos, string text) {
            if (OnEventRaised != null)
                OnEventRaised.Invoke(pos, text);
        }
    }
}