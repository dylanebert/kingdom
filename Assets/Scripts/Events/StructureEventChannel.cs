using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Structures;
using Pointers;

namespace Events
{
    [System.Serializable]
    public class StructureEvent : UnityEvent<Structure> { }

    [CreateAssetMenu(fileName = "StructureEventChannel", menuName = "Scriptable Objects/Events/Structure Event Channel")]
    public class StructureEventChannel : ScriptableObject
    {
        public UnityAction<Structure> OnEventRaised;

        public void Raise(Structure structure) {
            OnEventRaised?.Invoke(structure);
        }
    }
}