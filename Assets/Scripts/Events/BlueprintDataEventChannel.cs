using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Structures;
using Pointers;

namespace Events
{
    [System.Serializable]
    public class BlueprintDataEvent : UnityEvent<BlueprintData> { }

    [CreateAssetMenu(fileName = "BlueprintDataEventChannel", menuName = "Scriptable Objects/Events/Blueprint Data Event Channel")]
    public class BlueprintDataEventChannel : ScriptableObject
    {
        public UnityAction<BlueprintData> OnEventRaised;

        public void Raise(BlueprintData structure) {
            OnEventRaised?.Invoke(structure);
        }
    }
}