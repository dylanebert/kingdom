using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CitizenFactory", menuName = "Scriptable Objects/AI/Citizen Factory")]
public class CitizenInstanceFactory : Factory<CitizenInstance>
{
    public CitizenInstance prefab;

    public override CitizenInstance Create() {
        return Instantiate(prefab);
    }
}
