using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime;
using Logistics;
using Jobs;
using Events;
using Pathfinding;
using UnityEngine.Events;

public class CitizenInstance : MonoBehaviour
{
    public UnityAction OnInitialized;

    [HideInInspector] public Citizen citizen;

    public void Initialize(Citizen citizen) {
        this.citizen = citizen;
        citizen.instance = this;
        OnInitialized?.Invoke();
    }
}
