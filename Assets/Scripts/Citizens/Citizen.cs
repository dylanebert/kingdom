using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Events;
using UnityEngine.Events;

[System.Serializable]
public class Citizen
{
    [System.NonSerialized] public CitizenInstance instance;

    public event UnityAction<Citizen> OnDestroyed;

    public void Destroy() {
        OnDestroyed?.Invoke(this);
        Object.Destroy(instance.gameObject);
    }
}
