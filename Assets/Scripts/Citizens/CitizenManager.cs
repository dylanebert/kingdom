using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Events;
using Pointers;

public class CitizenManager : MonoBehaviour
{
    [SerializeField] CitizenInstancePool pool;
    [SerializeField] CellEventChannel createCitizenChannel;
    [SerializeField] int initialSize = 100;

    private void OnEnable() {
        createCitizenChannel.OnEventRaised += CreateCitizen;

        pool.Prewarm(initialSize);
        pool.SetParent(transform);
    }

    private void OnDisable() {
        createCitizenChannel.OnEventRaised -= CreateCitizen;

        pool.OnDisable();
    }

    public void CreateCitizen(Cell cell) {
        Citizen citizen = new Citizen();
        CitizenInstance citizenInstance = pool.Request();
        Vector3 pos = cell.WorldPos + Random.insideUnitSphere * 2;
        pos = new Vector3(pos.x, 0, pos.z);
        citizenInstance.transform.position = pos;
        citizenInstance.Initialize(citizen);
        
    }
}
