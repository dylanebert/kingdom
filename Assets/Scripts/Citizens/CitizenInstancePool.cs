using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CitizenPool", menuName = "Scriptable Objects/AI/Citizen Pool")]
public class CitizenInstancePool : ComponentPool<CitizenInstance>
{
    [SerializeField] CitizenInstanceFactory factory;

    public override IFactory<CitizenInstance> Factory {
        get {
            return factory;
        } set {
            factory = value as CitizenInstanceFactory;
        }
    }
}
