using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Audio
{
    [CreateAssetMenu(fileName = "SoundEmitterFactory", menuName = "Scriptable Objects/Audio/Sound Emitter Factory")]
    public class SoundEmitterFactory : Factory<SoundEmitter>
    {
        public SoundEmitter prefab;

        public override SoundEmitter Create() {
            return Instantiate(prefab);
        }
    }
}