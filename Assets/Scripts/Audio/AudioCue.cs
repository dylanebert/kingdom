using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Events;

namespace Audio
{
    [CreateAssetMenu(fileName = "AudioCue", menuName = "Scriptable Objects/Audio/Audio Cue")]
    public class AudioCue : ScriptableObject
    {
        [Header("Sound definition")]
        [SerializeField] AudioGroup group;

        [Header("Configuration")]
        [SerializeField] AudioEventChannel channel;
        [Range(0f, 1f)] public float volume = .5f;
        [Range(-3f, 3f)] public float pitch = 1f;
        [Range(0f, 1f)] public float pitchRandomization = 0f;
        [Range(0f, 1f)] public float spatialBlend = 1f;
        public bool loop = false;

        public void OnValidate() {
            if(group.clips.Length == 0)
                Debug.LogError("No clips set on AudioCue " + name);
        }

        public void Apply(AudioSource source) {
            source.clip = group.Next();
            source.volume = volume;
            source.pitch = pitch + Random.Range(-pitchRandomization, pitchRandomization);
            source.spatialBlend = spatialBlend;
            source.loop = loop;
        }

        public void PlayAudioCue() {
            channel.Raise(this);
        }

        public void PlayAudioCue(Vector3 pos) {
            channel.Raise(this, pos);
        }

        public void PlayAudioCue(Cell cell) {
            channel.Raise(this, cell.WorldPos);
        }
    }

    [System.Serializable]
    public class AudioGroup
    {
        public SequenceMode sequenceMode = SequenceMode.RandomNoRepeat;
        public AudioClip[] clips;

        int next = -1;
        int prev = -1;

        public AudioClip Next() {
            if (clips.Length == 1)
                return clips[0];

            if(next == -1) {
                next = (sequenceMode == SequenceMode.Sequential) ? 0 : Random.Range(0, clips.Length);
            } else {
                switch(sequenceMode) {
                    case SequenceMode.Random:
                        next = Random.Range(0, clips.Length);
                        break;
                    case SequenceMode.RandomNoRepeat:
                        do {
                            next = Random.Range(0, clips.Length);
                        } while (next == prev);
                        break;
                    case SequenceMode.Sequential:
                        next = (int)Mathf.Repeat(++next, clips.Length);
                        break;
                }
            }
            prev = next;
            return clips[next];
        }

        public enum SequenceMode
        {
            Random,
            RandomNoRepeat,
            Sequential
        }
    }
}