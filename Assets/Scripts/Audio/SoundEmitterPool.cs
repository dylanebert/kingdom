using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Audio
{
    [CreateAssetMenu(fileName = "SoundEmitterPool", menuName = "Scriptable Objects/Audio/Sound Emitter Pool")]
    public class SoundEmitterPool : ComponentPool<SoundEmitter>
    {
        [SerializeField] SoundEmitterFactory factory;

        public override IFactory<SoundEmitter> Factory {
            get {
                return factory;
            } set {
                factory = value as SoundEmitterFactory;
            }
        }
    }
}