using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Audio;

namespace Audio
{
    [RequireComponent(typeof(AudioSource))]
    public class SoundEmitter : MonoBehaviour
    {
        AudioSource source;

        public event UnityAction<SoundEmitter> onFinishedPlaying;

        private void Awake() {
            source = GetComponent<AudioSource>();
            source.playOnAwake = false;
        }

        public void PlayAudioCue(AudioCue cue, AudioMixerGroup group, Vector3 pos = default) {
            cue.Apply(source);
            source.outputAudioMixerGroup = group;
            source.transform.position = pos;
            source.Play();
            if (!source.loop)
                StartCoroutine(FinishedCoroutine(source.clip.length));
        }

        public void Resume() { source.Play(); }
        public void Pause() { source.Pause(); }
        public void Stop() { source.Stop(); }
        public bool IsPlaying => source.isPlaying;
        public bool IsLooping => source.loop;

        IEnumerator FinishedCoroutine(float length) {
            yield return new WaitForSecondsRealtime(length + 1);
            onFinishedPlaying.Invoke(this);
        }
    }
}