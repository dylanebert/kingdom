using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using Events;

namespace Audio {
    public class AudioManager : MonoBehaviour
    {
        [Header("Emitter")]
        [SerializeField] SoundEmitterPool pool;
        [SerializeField] int initialSize = 10;

        [Header("Listening on channels")]
        [SerializeField] AudioEventChannel sfxChannel;

        [Header("Audio control")]
        [SerializeField] AudioMixer mixer;
        [SerializeField] AudioMixerGroup sfxGroup;
        [SerializeField] float masterVolume = 1f;
        [SerializeField] float sfxVolume = 1f;

        private void OnEnable() {
            sfxChannel.OnAudioRequested += PlaySFXAudioCue;

            pool.Prewarm(initialSize);
            pool.SetParent(transform);
        }

        private void OnValidate() {
            if(Application.isPlaying) {
                SetGroupVolume("MasterVolume", masterVolume);
                SetGroupVolume("SFXVolume", sfxVolume);
            }
        }

        private void OnDisable() {
            sfxChannel.OnAudioRequested -= PlaySFXAudioCue;
        }

        public void SetGroupVolume(string parameterName, float normalizedVolume) {
            bool volumeSet = mixer.SetFloat(parameterName, NormalizedToMixerValue(normalizedVolume));
            if (!volumeSet)
                Debug.LogError(string.Format("AudioMixer parameter {0} not found.", parameterName));
        }

        public float GetGroupVolume(string parameterName) {
            if (mixer.GetFloat(parameterName, out float volume))
                return MixerValueToNormalized(volume);
            else {
                Debug.LogError(string.Format("AudioMixer parameter {0} not found.", parameterName));
                return 0;
            }
        }

        float MixerValueToNormalized(float mixerValue) {
            return 1f + (mixerValue / 80f);
        }

        float NormalizedToMixerValue(float normalizedValue) {
            return (normalizedValue - 1f) * 80f;
        }

        public void PlaySFXAudioCue(AudioCue cue, Vector3 pos = default) {
            SoundEmitter emitter = pool.Request();
            emitter.PlayAudioCue(cue, sfxGroup, pos);
            emitter.onFinishedPlaying += OnEmitterFinishedPlaying;
        }

        public void OnEmitterFinishedPlaying(SoundEmitter emitter) {
            emitter.onFinishedPlaying -= OnEmitterFinishedPlaying;
            emitter.Stop();
            pool.Return(emitter);
        }
    }
}