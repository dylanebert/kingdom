using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Pool<T> : ScriptableObject, IPool<T>
{
    protected readonly Stack<T> Available = new Stack<T>();
    
    public abstract IFactory<T> Factory { get; set; }
    protected bool Prewarmed { get; set; }

    protected virtual T Create() {
        return Factory.Create(); 
    }

    public virtual void Prewarm(int n) {
        if (Prewarmed) {
            Debug.LogWarning("Pool has already been prewarmed.");
            return;
        }
        for (int i = 0; i < n; i++)
            Available.Push(Create());
        Prewarmed = true;
    }

    public virtual T Request() {
        return Available.Count > 0 ? Available.Pop() : Create();
    }

    public virtual IEnumerable<T> Request(int n = 1) {
        List<T> members = new List<T>(n);
        for (int i = 0; i < n; i++)
            members[i] = Request();
        return members;
    }

    public virtual void Return(T member) {
        Available.Push(member);
    }

    public virtual void Return(IEnumerable<T> members) {
        foreach (T member in members)
            Return(member);
    }

    public virtual void OnDisable() {
        Available.Clear();
        Prewarmed = false;
    }
}
