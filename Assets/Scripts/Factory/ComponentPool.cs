using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ComponentPool<T> : Pool<T> where T : Component
{
    Transform _poolRoot;
    Transform PoolRoot {
        get {
            if(_poolRoot == null) {
                _poolRoot = new GameObject(name).transform;
                _poolRoot.SetParent(_parent);
            }
            return _poolRoot;
        }
    }

    Transform _parent;

    public void SetParent(Transform t) {
        _parent = t;
        PoolRoot.SetParent(_parent);
    }

    public override T Request() {
        T member = base.Request();
        member.gameObject.SetActive(true);
        return member;
    }

    public override void Return(T member) {
        member.transform.SetParent(PoolRoot.transform);
        member.gameObject.SetActive(false);
        base.Return(member);
    }

    protected override T Create() {
        T member = base.Create();
        member.transform.SetParent(PoolRoot.transform);
        member.gameObject.SetActive(false);
        return member;
    }

    public override void OnDisable() {
        base.OnDisable();
        if (_poolRoot != null) {
#if UNITY_EDITOR
            DestroyImmediate(_poolRoot.gameObject);
#else
            Destroy(_poolRoot.gameObject);
#endif
        }
    }
}
