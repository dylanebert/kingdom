using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Events;
using Procedural;

public class GameController : MonoBehaviour
{
    [SerializeField] VoidEventChannel generateChannel;
    [SerializeField] VoidEventChannel loadChannel;
    [SerializeField] VoidEventChannel saveChannel;
    [SerializeField] ProceduralData proceduralData;

    public void OnValidate() {
        if (generateChannel == null || loadChannel == null || saveChannel == null || proceduralData == null)
            Debug.LogWarning("Missing references on GameController");
    }

    bool busy = false;

    private void Start() {
        generateChannel.Raise();
    }

    private void Update() {
        if (busy) return;

        if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.R)) {
            AsyncOperation callback = SceneManager.UnloadSceneAsync(1);
            callback.completed += Reload;
            busy = true;
        }
        if (Input.GetKeyDown(KeyCode.F5)) {
            busy = true;
            Save();
        }
        if (Input.GetKeyDown(KeyCode.F8)) {
            AsyncOperation callback = SceneManager.UnloadSceneAsync(1);
            callback.completed += Load;
            busy = true;
        }

        if (Input.GetKeyDown(KeyCode.Alpha1))
            Time.timeScale = 1f;
        if (Input.GetKeyDown(KeyCode.Alpha2))
            Time.timeScale = 3f;
        if (Input.GetKeyDown(KeyCode.Alpha3))
            Time.timeScale = 20f;
        if (Input.GetKeyDown(KeyCode.Alpha0))
            Time.timeScale = 0f;
    }

    void Save() {
        saveChannel.Raise();
        busy = false;
    }

    void Load(AsyncOperation callback) {
        callback.completed -= Load;
        AsyncOperation loadCallback = SceneManager.LoadSceneAsync(1, LoadSceneMode.Additive);
        loadCallback.completed += OnLoad;        
    }

    void OnLoad(AsyncOperation callback) {
        callback.completed -= OnLoad;
        loadChannel.Raise();
        busy = false;
    }

    void Reload(AsyncOperation callback) {
        callback.completed -= Reload;
        AsyncOperation loadCallback = SceneManager.LoadSceneAsync(1, LoadSceneMode.Additive);
        loadCallback.completed += Generate;
    }

    void Generate(AsyncOperation callback) {
        callback.completed -= Generate;
        generateChannel.Raise();
        busy = false;
    }
}
