using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Events;

namespace FX
{
    public class ParticleManager : MonoBehaviour
    {
        [Header("Listening on channels")]
        [SerializeField] ParticleEventChannel particleChannel;

        private void OnEnable() {
            particleChannel.OnParticleRequested += PlayParticleCue;
        }

        private void OnDisable() {
            particleChannel.OnParticleRequested -= PlayParticleCue;
        }

        public void PlayParticleCue(ParticleCue cue, Vector3 pos) {
            StartCoroutine(PlayParticleCueCoroutine(cue, pos));
        }

        IEnumerator PlayParticleCueCoroutine(ParticleCue cue, Vector3 pos) {
            ParticleSystem particle = Instantiate(cue.prefab);
            particle.transform.position = pos;
            particle.Play();
            yield return new WaitForSeconds(particle.main.duration);
            particle.Stop(true, ParticleSystemStopBehavior.StopEmitting);
            yield return new WaitUntil(() => particle.particleCount == 0);
            Destroy(particle.gameObject);
        }
    }
}