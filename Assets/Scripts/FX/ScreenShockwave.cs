using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Events;

namespace FX
{
    public class ScreenShockwave : MonoBehaviour
    {
        [SerializeField] ScreenShock blit;
        [SerializeField] FloatEventChannel channel;

        private void OnEnable() {
            channel.OnEventRaised += Shockwave;
        }

        private void OnDisable() {
            channel.OnEventRaised -= Shockwave;
        }

        private void Update() {
            if (blit.phase != -1) {
                blit.phase += Time.unscaledDeltaTime * 2;
                if (blit.phase >= 1)
                    blit.phase = -1;
            }
        }

        public void Shockwave(float magnitude) {
            blit.magnitude = magnitude * magnitude * .5f;
            blit.phase = 0;
        }
    }
}