using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Events;

namespace FX
{
    public class ScreenShake : MonoBehaviour
    {
        [SerializeField] FloatEventChannel channel;

        public void OnValidate() {
            if (channel == null)
                Debug.LogWarning("Missing references on Screen Shake");
        }

        Transform tr;
        float shake;

        private void Awake() {
            tr = transform;
        }

        private void OnEnable() {
            channel.OnEventRaised += Shake;
        }

        private void OnDisable() {
            channel.OnEventRaised -= Shake;
        }

        private void Update() {
            if (shake > 0) {
                shake = Mathf.Max(0, shake - Time.unscaledDeltaTime);
                tr.localRotation = Quaternion.Euler(Random.insideUnitSphere * .5f * shake * shake);
            }
        }

        public void Shake(float magnitude) {
            shake = Mathf.Max(shake, magnitude);
        }
    }
}