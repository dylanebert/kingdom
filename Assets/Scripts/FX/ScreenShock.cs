using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

namespace FX {
    public class ScreenShock : Blit
    {
        [NonSerialized] public float phase;
        [NonSerialized] public float magnitude;

        public override void AddRenderPasses(ScriptableRenderer renderer, ref RenderingData renderingData) {
            if (phase == -1)
                return;
            settings.blitMaterial.SetFloat("_Phase", phase);
            settings.blitMaterial.SetFloat("_Magnitude", magnitude);
            base.AddRenderPasses(renderer, ref renderingData);
        }
    }
}