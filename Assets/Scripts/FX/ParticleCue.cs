using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Events;

namespace FX
{
    [CreateAssetMenu(fileName = "ParticleCue", menuName = "Scriptable Objects/FX/Particle Cue")]
    public class ParticleCue : ScriptableObject
    {
        [Header("Particle definition")]
        public ParticleSystem prefab;

        [Header("Configuration")]
        [SerializeField] ParticleEventChannel channel;

        public void PlayParticleCue(Vector3 pos) {
            channel.Raise(this, pos);
        }
        
        public void PlayParticleCue(Cell cell) {
            channel.Raise(this, cell.WorldPos);
        }
    }
}