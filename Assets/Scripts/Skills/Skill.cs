using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Events;

namespace Skills
{
    [CreateAssetMenu(fileName = "Skill", menuName = "Scriptable Objects/Skills/Skill")]
    [System.Serializable]
    public class Skill : ScriptableObject
    {
        public float output;

        [SerializeField] StringEvent onLevelUp;

        [SerializeField] int level;
        [SerializeField] float experience;

        [SerializeField] float conversionMin;
        [SerializeField] float conversionMax;

        private void OnEnable() {
            level = 0;
            experience = 0;
            output = conversionMin;
        }

        public void Contribute() {
            experience += 1f;
            if(experience >= ExperienceCurve.Evaluate(level)) {
                experience = 0;
                LevelUp();
            }
        }

        void LevelUp() {
            level = Mathf.Clamp(level + 1, 0, 100);
            output = Mathf.Lerp(conversionMin, conversionMax, level / 100f);
            onLevelUp?.Invoke(string.Format("{0} skill increased to {1}", name, level));
        }
    }
}