using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Structures;
using Terrain;

public static class SaveSystem
{
    public static void SaveTerrain(CellRuntimeSet cells) {
        string path = Application.persistentDataPath + "/terrain.json";
        File.WriteAllText(path, JsonUtility.ToJson(cells));
    }

    public static void LoadTerrain(CellRuntimeSet cells) {
        string path = Application.persistentDataPath + "/terrain.json";
        if(File.Exists(path)) {
            string json = File.ReadAllText(path);
            cells.Initialize(json);
        } else {
            Debug.LogError("Save file not found at path " + path);
        }
    }

    public static void SaveStructures(StructureRuntimeSet structures) {
        string path = Application.persistentDataPath + "/structures.json";
        Debug.Log(JsonUtility.ToJson(structures.structures[0]));
        File.WriteAllText(path, JsonUtility.ToJson(structures));
    }

    public static void LoadStructures(StructureRuntimeSet structures) {
        string path = Application.persistentDataPath + "/structures.json";
        if (File.Exists(path)) {
            string json = File.ReadAllText(path);
            structures.Initialize(json);
        }
        else {
            Debug.LogError("Save file not found at path " + path);
        }
    }
}
