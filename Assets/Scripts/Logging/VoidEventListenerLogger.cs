using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Events;

namespace Logging {
    public class VoidEventListenerLogger : MonoBehaviour
    {
        [SerializeField] VoidEventChannel channel;

        private void OnEnable() {
            channel.OnEventRaised += Log;
        }

        private void OnDisable() {
            channel.OnEventRaised -= Log;
        }

        public void Log() {
            Debug.Log("Void event on channel " + channel.name + " invoked");
        }
    }
}