using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BehaviorDesigner.Runtime.Tasks 
{ 
    public class TestInterrupt : Conditional
    {
        public override TaskStatus OnUpdate() {
            return Input.GetKeyDown(KeyCode.T) ? TaskStatus.Success : TaskStatus.Failure;
        }
    }
}
