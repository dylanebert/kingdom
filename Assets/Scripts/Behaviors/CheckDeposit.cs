using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Logistics;
using Events;

namespace BehaviorDesigner.Runtime.Tasks
{
    public class CheckDeposit : Conditional
    {
        public SharedString resource;
        public SharedGameObject target;

        List<Transporter> transporters = new List<Transporter>();

        public override void OnAwake() {
            foreach (TransporterInstance instance in gameObject.GetComponents<TransporterInstance>())
                transporters.Add(instance.transporter);
        }

        public override TaskStatus OnUpdate() {
            for(int i = 0; i < transporters.Count; i++) {
                if(transporters[i].carrying.Count > 0) {
                    resource.Value = transporters[i].resource;
                    target.Value = transporters[i].carrying[0].destination.Instance;
                    return TaskStatus.Success;
                }
            }
            return TaskStatus.Failure;
        }
    }
}