using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Structures;

namespace BehaviorDesigner.Runtime.Tasks.Movement.AstarPathfindingProject
{
    public class SeekRandomNearPosition : Seek
    {
        public SharedGameObject origin;
        public float magnitude = 2;

        public override void OnStart() {
            Vector3 pos = origin.Value.transform.position + Random.insideUnitSphere * magnitude;
            pos = new Vector3(pos.x, 0, pos.z);
            targetPosition.Value = pos;
            target.Value = null;
            base.OnStart();
        }
    }
}