using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jobs;

namespace BehaviorDesigner.Runtime.Tasks
{
    public class RequestJob : Conditional
    {
        public SharedGameObject target;

        [SerializeField] JobRuntimeSet jobRuntimeSet; 

        Worker worker;

        public override void OnAwake() {
            worker = GetComponent<WorkerInstance>().worker;
        }

        public override TaskStatus OnUpdate() {
            if(worker.activeJob == null) {
                if(jobRuntimeSet.Request(out Job job)) {
                    worker.RegisterJob(job);
                    target.Value = job.instance;
                    return TaskStatus.Success;
                }
            }
            return TaskStatus.Failure;
        }
    }
}