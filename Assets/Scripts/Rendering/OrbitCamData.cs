using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "OrbitCamData", menuName = "Scriptable Objects/Rendering/Orbit Cam Data")]
public class OrbitCamData : ScriptableObject
{
    public float panSpeed = 30;
    public float zoomSpeed = 7;
    public float rotSpeed = 3;
    public float dampening = 15;
    public float minDistance = 10;
    public float maxDistance = 400;
    public float startDistance = 300;
}
