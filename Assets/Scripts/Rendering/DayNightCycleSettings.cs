using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName="Day Night Cycle Settings", menuName="Scriptable Objects/Rendering/Day Night Cycle Settings", order=1)]
public class DayNightCycleSettings : ScriptableObject
{
    [GradientUsage(true)]
    public Gradient AmbientColor;
    [GradientUsage(true)]
    public Gradient DirectionalColor;
    [GradientUsage(true)]
    public Gradient SkyColor;
    [GradientUsage(true)]
    public Gradient GroundColor;
    [GradientUsage(true)]
    public Gradient WaterColor;
    public Gradient WaterIntersectionColor;
    public AnimationCurve AtmosphereThickness;
    public float LightIntensity;
    public float ShadowIntensity;
}