using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class DayNightCycle : MonoBehaviour
{
    static float DayLength = 720; //12 minutes
    static float NightLength = 300; //5 minutes

    [SerializeField] DayNightCycleSettings settings;
    [SerializeField] Light sun;
    [SerializeField] Light moon;    
    [SerializeField] Material skybox;
    [SerializeField] Material water;
    [SerializeField, Range(0, 24)] float timeOfDay;

    public bool IsDay => timeOfDay > 6 && timeOfDay < 18;

    bool play;

    private void OnEnable() {
        play = true;
        timeOfDay = 10;
    }

    void Update() {
        if (settings == null || skybox == null || sun == null || moon == null || water == null)
            return;

        if (Input.GetKeyDown(KeyCode.L))
            play = !play;

        if(Application.isPlaying && play) {
            if(IsDay) {

            }
            if (IsDay)
                timeOfDay += Time.deltaTime * 12 / DayLength;
            else
                timeOfDay += Time.deltaTime * 12 / NightLength;
            timeOfDay %= 24;
        }

        UpdateLighting(Evaluate(timeOfDay / 24f));
    }

    void UpdateLighting(float t) {
        RenderSettings.ambientLight = settings.AmbientColor.Evaluate(t);
        skybox.SetColor("_SkyTint", settings.SkyColor.Evaluate(t));
        skybox.SetColor("_GroundColor", settings.GroundColor.Evaluate(t));
        skybox.SetFloat("_AtmosphereThickness", settings.AtmosphereThickness.Evaluate(t));
        water.SetColor("_BaseColor", settings.WaterColor.Evaluate(t));
        water.SetColor("_ShallowColor", settings.WaterColor.Evaluate(t));
        water.SetColor("_HorizonColor", settings.WaterColor.Evaluate(t));
        water.SetColor("_IntersectionColor", settings.WaterIntersectionColor.Evaluate(t));

        if (t > .75f || t < .25f) {
            sun.enabled = false;
            moon.enabled = true;
            moon.intensity = settings.LightIntensity;
            moon.color = settings.DirectionalColor.Evaluate(t);
        }
        else {
            sun.enabled = true;
            moon.enabled = false;
            sun.intensity = settings.LightIntensity;
            sun.shadowStrength = settings.ShadowIntensity;
            sun.color = settings.DirectionalColor.Evaluate(t);
        }

        Quaternion dir = Quaternion.AngleAxis(270, Vector3.up);
        Quaternion tilt = Quaternion.AngleAxis(-35, Vector3.forward);
        Quaternion sunRot = Quaternion.AngleAxis(t * 360 - 90, Vector3.right);
        Quaternion moonRot = Quaternion.AngleAxis(t * 360 - 270, Vector3.right);
        sun.transform.rotation = dir * tilt * sunRot;
        moon.transform.rotation = dir * tilt * moonRot;
    }

    float Evaluate(float t) {
        float t0, v;
        if(t < .5f) {
            t0 = 2 * t;
            v = Easing.EaseInOutExpo(t0) / 2f;
        } else {
            t0 = 2 * t - 1;
            v = (Easing.EaseInOutExpo(t0) + 1) / 2f;
        }
        return v;
    }
}
