﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class OrbitCam : MonoBehaviour
{
    [SerializeField] OrbitCamData data;
    [SerializeField] Transform target;
    [SerializeField] Transform fogPlane;
    [SerializeField] CameraFocusVariable focusVariable;

    Quaternion targetRotation;
    Vector3 dampenedTarget;
    float currentDistance;
    float targetDistance;
    float xDeg;
    float yDeg;
    int clampSize;

    private void Start() {
        targetDistance = currentDistance = data.startDistance;
        xDeg = Vector3.Angle(Vector3.right, transform.right);
        yDeg = Vector3.Angle(Vector3.up, transform.up);
        targetRotation = transform.rotation = Quaternion.Euler(yDeg, xDeg, 0);
        dampenedTarget = target.transform.position;
        clampSize = 1024;
    }

    private void Update() {
        float multiplier = .83f * Mathf.InverseLerp(data.minDistance, data.maxDistance, targetDistance) + .17f;

        //Mouse controls
        if (!EventSystem.current.IsPointerOverGameObject()) {
            targetDistance += Input.GetAxis("Mouse ScrollWheel") * Time.unscaledDeltaTime * data.zoomSpeed * multiplier * -10000;
            targetDistance = Mathf.Clamp(targetDistance, data.minDistance, data.maxDistance);

            if (Input.GetMouseButton(2)) {
                xDeg += Input.GetAxis("Mouse X") * data.rotSpeed;
                yDeg -= Input.GetAxis("Mouse Y") * data.rotSpeed;

                xDeg = ClampAngle(xDeg, 0, 360);
                yDeg = ClampAngle(yDeg, 5, 85);

                targetRotation = Quaternion.Euler(yDeg, xDeg, 0);
            }
        }

        //Keyboard controls
        target.rotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0);
        Vector3 pan = Vector3.zero;
        if (Input.GetKey(KeyCode.W)) {
            pan += Vector3.forward;
        }
        if (Input.GetKey(KeyCode.A)) {
            pan += Vector3.left;
        }
        if (Input.GetKey(KeyCode.S)) {
            pan += Vector3.back;
        }
        if (Input.GetKey(KeyCode.D)) {
            pan += Vector3.right;
        }
        if (pan.magnitude > 0) {
            target.Translate(pan.normalized * Time.unscaledDeltaTime * data.panSpeed * multiplier * 10, Space.Self);
            target.position = new Vector3(Mathf.Clamp(target.position.x, -clampSize, clampSize), 0, Mathf.Clamp(target.position.z, -clampSize, clampSize));
        }

        if (Input.GetKeyDown(KeyCode.Q)) {
            xDeg = ClampAngle(xDeg + 90, 0, 360);
            targetRotation = Quaternion.Euler(yDeg, xDeg, 0);
        }
        else if (Input.GetKeyDown(KeyCode.E)) {
            xDeg = ClampAngle(xDeg - 90, 0, 360);
            targetRotation = Quaternion.Euler(yDeg, xDeg, 0);
        }

        currentDistance = Mathf.Lerp(currentDistance, targetDistance, Time.unscaledDeltaTime * data.dampening);
        Quaternion rot = Quaternion.Lerp(transform.rotation, targetRotation, Time.unscaledDeltaTime * data.dampening);
        transform.rotation = Quaternion.Euler(rot.eulerAngles.x, rot.eulerAngles.y, 0);

        dampenedTarget = Vector3.Lerp(dampenedTarget, target.transform.position, Time.unscaledDeltaTime * data.dampening);
        transform.localPosition = dampenedTarget - (transform.forward * currentDistance);
        focusVariable.pos = transform.position;

        fogPlane.position = new Vector3(fogPlane.position.x, Mathf.Lerp(0f, 10f, Mathf.Clamp01(transform.position.y / 20f)), fogPlane.position.z);
    }

    float ClampAngle(float angle, float min, float max) {
        if (angle < 0)
            angle += 360;
        else if (angle > 360)
            angle -= 360;
        return Mathf.Clamp(angle, min, max);
    }

    public float FocusMagnitude(Transform focus) {
        float dx = 1 - Mathf.InverseLerp(0, 30, Vector3.Distance(target.position, focus.position));
        float dz = 1 - Mathf.InverseLerp(data.minDistance, data.minDistance + 30, currentDistance);
        return dx * dz;
    }
}
