﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Pathfinding;
using Structures;
using UnityEngine.Events;
using Terrain;

namespace Procedural
{
    [CreateAssetMenu(fileName = "ProceduralData", menuName = "Scriptable Objects/Procedural Data")]
    public class ProceduralData : ScriptableObject
    {
        public UnityAction<Cell[,]> OnTerrainGenerated;

        [SerializeField] CellData ocean;
        [SerializeField] CellData land;
        [SerializeField] CellData river;

        [Tooltip("Whether to use a random seed")]
        [SerializeField] bool useRandomSeed;
        [Tooltip("Seed with which to generate the world")]
        [SerializeField] int seed;
        [Tooltip("Number of chunks in each direction")]
        [Range(1, 20)] public int numChunks = 4;
        [Tooltip("More octaves results in more fine-grained detail in the heightmap")]
        [Range(1, 5)] [SerializeField] int octaves = 3;
        [Tooltip("Number of rivers to carve")]
        [Range(0, 50)] [SerializeField] int rivers = 5;
        [Tooltip("Height of the water")]
        [Range(0f, 1f)] [SerializeField] float waterLevel = .3f;
        [Tooltip("Global scale of the heightmap, controlling granularity")]
        [Range(1f, 100f)] [SerializeField] float scale = 40;
        [Tooltip("Scale of noise generating stones")]
        [Range(1f, 100f)] [SerializeField] float stoneScale = 5;
        [Tooltip("Scale of noise generating trees")]
        [Range(1f, 100f)] [SerializeField] float treeScale = 10;
        [Tooltip("Amount of detail added or removed at each octave")]
        [Range(0f, 1f)] [SerializeField] float persistance = .5f;
        [Tooltip("How much each octave contributes to overall amplitude")]
        [Range(1f, 3f)] [SerializeField] float lacunarity = 1.9f;

        System.Random m_rand;
        float[,] m_heightMap;
        Cell[,] m_cells;
        List<Structure> m_structures;
        int _rivers;
        int size;

        private void OnEnable() {
            size = numChunks * TerrainRenderer.CHUNK_SIZE;
            if (useRandomSeed)
                m_rand = new System.Random(UnityEngine.Random.Range(-100000, 100000));
            else
                m_rand = new System.Random(seed);
        }

        public void GenerateTerrain() {        
            m_heightMap = GenerateHeightmap();
            m_cells = ContinentFromHeightmap();
            CreateGridGraph(m_cells);
            CarveRivers();
        }

        Cell[,] ContinentFromHeightmap() {
            Cell[,] cells = new Cell[size, size];
            for (int y = 0; y < size; y++) {
                for (int x = 0; x < size; x++) {
                    float v = m_heightMap[x, y];
                    if (v < waterLevel)
                        cells[x, y] = new Cell(ocean, x, y);
                    else
                        cells[x, y] = new Cell(land, x, y);
                }
            }
            return cells;
        }

        float[,] GenerateHeightmap() {
            //Generate perlin noisemap octaves
            float[,] noisemap = new float[size, size];
            Vector2[] offsets = new Vector2[octaves];
            for (int i = 0; i < octaves; i++) {
                float x = m_rand.Next(-100000, 100000);
                float y = m_rand.Next(-100000, 100000);
                offsets[i] = new Vector2(x, y);
            }

            //Merge octaves into final noisemap
            float min = float.MaxValue;
            float max = float.MinValue;
            for (int y = 0; y < size; y++) {
                for (int x = 0; x < size; x++) {
                    float amplitude = 1f;
                    float frequency = 1f;
                    float height = 0f;
                    for (int i = 0; i < octaves; i++) {
                        float xv = (x - size / 2) / scale * frequency + offsets[i].x;
                        float yv = (y - size / 2) / scale * frequency + offsets[i].y;
                        float v = Mathf.PerlinNoise(xv, yv) * 2 - 1;
                        height += v * amplitude;
                        amplitude *= persistance;
                        frequency *= lacunarity;
                    }
                    if (height > max)
                        max = height;
                    else if (height < min)
                        min = height;
                    noisemap[x, y] = height;
                }
            }

            //Compute falloff
            float[,] falloff = new float[size, size];
            for (int y = 0; y < size; y++) {
                for (int x = 0; x < size; x++) {
                    float xv = x / (float)size * 2 - 1;
                    float yv = y / (float)size * 2 - 1;
                    float v = Mathf.Max(Mathf.Abs(xv), Mathf.Abs(yv));
                    v = Mathf.Pow(v, 3f) / (Mathf.Pow(v, 3f) + Mathf.Pow(2.2f - 2.2f * v, 3f));
                    falloff[x, y] = v;
                }
            }

            //Merge noisemap with falloff to create heightmap
            float[,] heightMap = new float[size, size];
            for (int y = 0; y < size; y++) {
                for (int x = 0; x < size; x++) {
                    float v = Mathf.InverseLerp(min, max, noisemap[x, y]);
                    v -= falloff[x, y];
                    heightMap[x, y] = Mathf.Max(v, 0);
                }
            }

            return heightMap;
        }

        public void CreateGridGraph(Cell[,] cells) {
            float[,] noiseMap = new float[size, size];
            Vector2 offset = new Vector2(m_rand.Next(-100000, 100000), m_rand.Next(-100000, 100000));
            for (int y = 0; y < size; y++) {
                for (int x = 0; x < size; x++) {
                    float xv = (x - size / 2) / scale * 7 + offset.x;
                    float yv = (y - size / 2) / scale * 7 + offset.y;
                    noiseMap[x, y] = Mathf.PerlinNoise(xv, yv);
                }
            }

            AstarData data = AstarPath.active.data;
            GridGraph gg = data.AddGraph(typeof(GridGraph)) as GridGraph;
            gg.neighbours = NumNeighbours.Four;
            gg.cutCorners = false;
            gg.center = new Vector3(size * 2 - 2, 0, size * 2 - 2);
            gg.SetDimensions(size, size, 4);
            AstarPath.active.Scan(gg);
            AstarData.active.AddWorkItem(new AstarWorkItem(ctx => {
                for (int y = 0; y < cells.GetLength(1); y++) {
                    for (int x = 0; x < cells.GetLength(0); x++) {
                        Cell cell = cells[x, y];
                        GraphNode node = gg.GetNode(x, y);
                        node.Walkable = noiseMap[x, y] > .4f;
                        node.Tag = cell.PathfindingTag;
                    }
                }
            }));
        }

        void CarveRivers() {
            _rivers = 0;
            GridGraph gg = AstarData.active.data.gridGraph;
            for (int i = 0; i < rivers; i++) {
                GraphNode start = gg.GetNode(m_rand.Next(0, size - 1), 0);
                GraphNode end = gg.GetNode(m_rand.Next(0, size - 1), size - 1);
                ABPath path = ABPath.Construct((Vector3)start.position, (Vector3)end.position, OnPathComplete);
                path.Claim(this);
                AstarPath.StartPath(path);
            }
        }

        void OnPathComplete(Path path) {
            path.Release(this);
            for (int i = 0; i < path.path.Count; i++) {
                GraphNode node = path.path[i];
                int x = node.NodeIndex % m_cells.GetLength(0);
                int y = node.NodeIndex / m_cells.GetLength(0);
                Cell cell = m_cells[x, y];
                if (cell.terrain == land.name) {
                    cell.Apply(river);
                    m_cells[x, y] = cell;
                }
            }
            if (++_rivers == rivers)
                OnRiversFinishedGenerating();
        }

        void OnRiversFinishedGenerating() {
            AddStones();
            AddTrees();
            OnTerrainGenerated?.Invoke(m_cells);
        }

        void AddStones() {
            float[,] noiseMap = new float[size, size];
            Vector2 offset = new Vector2(m_rand.Next(-100000, 100000), m_rand.Next(-100000, 100000));
            for (int y = 0; y < size; y++) {
                for (int x = 0; x < size; x++) {
                    float xv = (x - size / 2) / stoneScale + offset.x;
                    float yv = (y - size / 2) / stoneScale + offset.y;
                    noiseMap[x, y] = Mathf.PerlinNoise(xv, yv);
                }
            }

            bool[,] marked = new bool[size, size];
            for (int y = 0; y < size; y++) {
                for (int x = 0; x < size; x++) {
                    float v = m_rand.Next(0, 10);
                    if (!marked[x, y] && !m_cells[x, y].isWater) {
                        if (noiseMap[x, y] * 100 < v) {
                            m_cells[x, y].generationTag = "Stone";
                            for (int i = -6; i <= 6; i++) {
                                for (int j = -6; j <= 6; j++) {
                                    int x_ = Mathf.Clamp(x + i, 0, size - 1);
                                    int y_ = Mathf.Clamp(y + j, 0, size - 1);
                                    marked[x_, y_] = true;
                                }
                            }
                        }
                    }
                }
            }
        }

        void AddTrees() {
            float[,] noiseMap = new float[size, size];
            Vector2 offset = new Vector2(m_rand.Next(-100000, 100000), m_rand.Next(-100000, 100000));
            for (int y = 0; y < size; y++) {
                for (int x = 0; x < size; x++) {
                    float xv = (x - size / 2) / treeScale + offset.x;
                    float yv = (y - size / 2) / treeScale + offset.y;
                    noiseMap[x, y] = Mathf.PerlinNoise(xv, yv);
                }
            }

            for (int y = 0; y < size; y++) {
                for (int x = 0; x < size; x++) {
                    float v = m_rand.Next(0, 40);
                    if (!m_cells[x, y].isWater && m_cells[x, y].generationTag == null) {
                        if (noiseMap[x, y] * 100 < v) {
                            int treeType = Mathf.FloorToInt(Mathf.Clamp(m_rand.Next(-20, 25), 0, 29) / 10f);
                            m_cells[x, y].generationTag = new string[3] { "Pine", "Birch", "Fir" }[treeType];
                        }
                    }
                }
            }
        }

        public void FinalizeGridGraph(Cell[,] cells) {
            AstarData.active.AddWorkItem(new AstarWorkItem(ctx => {
                GridGraph gg = AstarData.active.data.gridGraph;
                for (int y = 0; y < cells.GetLength(1); y++) {
                    for (int x = 0; x < cells.GetLength(0); x++) {
                        Cell cell = cells[x, y];
                        GridNode node = gg.GetNode(x, y) as GridNode;
                        node.Walkable = cell.walkable;
                        node.Tag = cell.PathfindingTag;
                    }
                }
            }));
        }
    }
}