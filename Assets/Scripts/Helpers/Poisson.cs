using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Poisson
{
    public static int Sample(float lambda) {
        float p = 1f;
        float L = Mathf.Exp(-lambda);
        int k = 0;
        do {
            k++;
            p *= Random.Range(0f, 1f);
        } while (p > L);
        return Mathf.Max(1, k - 1);
    }
}
