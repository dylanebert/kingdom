using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FloatVariable", menuName = "Scriptable Objects/Variables/String Variable")]
public class StringVariable : ScriptableObject
{
    public string Value;
}
