using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CameraFocusVariable", menuName = "Scriptable Objects/Variables/Camera Focus Variable")]
public class CameraFocusVariable : ScriptableObject
{
    [SerializeField] float minDist = 10;
    [SerializeField] float maxDist = 100;

    [HideInInspector] public Vector3 pos;

    public float ComputeFocus(Vector3 target) {
        return Mathf.InverseLerp(maxDist, minDist, Vector3.Distance(pos, target));
    }
}
