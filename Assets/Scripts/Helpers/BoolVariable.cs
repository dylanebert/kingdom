using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FloatVariable", menuName = "Scriptable Objects/Variables/Bool Variable")]
public class BoolVariable : ScriptableObject
{
    public bool Value;
}
