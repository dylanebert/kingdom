using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Easing
{
    public static float Bounce(float t) {
        t = Mathf.Clamp01(t);
        float n1 = 7.5625f;
        float d1 = 2.75f;
        if (t < 1 / d1) {
            return n1 * t * t;
        }
        else if (t < 2 / d1) {
            return n1 * (t -= 1.5f / d1) * t + .75f;
        }
        else if (t < 2.5f / d1) {
            return n1 * (t -= 2.25f / d1) * t + .9375f;
        }
        else {
            return n1 * (t -= 2.625f / d1) * t + .984375f;
        }
    }

    public static float OutBack(float t) {
        t = Mathf.Clamp01(t);
        float c1 = 1.70158f;
        float c3 = c1 + 1;
        return 1 + c3 * Mathf.Pow(t - 1, 3) + c1 * Mathf.Pow(t - 1, 2);
    }

    public static float Exponential(float t) {
        t = Mathf.Clamp01(t);
        return 1 - Mathf.Pow(2, -10 * t);
    }

    public static float ContinuousBounce(float t) {
        t = Mathf.Clamp01(t);
        return .5f - .5f * Mathf.Cos(2 * t * Mathf.PI);
    }

    public static float Elastic(float t) {
        t = Mathf.Clamp01(t);
        return Mathf.Pow(2, -10 * t) * Mathf.Sin((t * 10 - .75f) * 2 * Mathf.PI / 3f) + 1;
    }

    public static float EaseInOutExpo(float t) {
        t = Mathf.Clamp01(t);
        if (t < .5f)
            return Mathf.Pow(2, 20 * t - 10) / 2f;
        else
            return (2 - Mathf.Pow(2, -20 * t + 10)) / 2f;
    }
}
