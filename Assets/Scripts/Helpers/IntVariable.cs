using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FloatVariable", menuName = "Scriptable Objects/Variables/Int Variable")]
public class IntVariable : ScriptableObject
{
    public int Value;
}
