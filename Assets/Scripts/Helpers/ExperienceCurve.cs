using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ExperienceCurve
{
    public static float Evaluate(int skill) {
        return Mathf.Pow(2, (skill + 180) / 30f) - 63;
    }
}
