using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Events;
using Structures;
using Pointers;

public class DestroyManager : MonoBehaviour
{
    [SerializeField] Texture2D destroyCursorTexture;
    [SerializeField] Pointer defaultPointer;
    [SerializeField] Pointer destroyPointer;
    [SerializeField] ButtonEventChannel toggleDestroyPointerEventChannel;
    [SerializeField] StringEventChannel showPointerWindowChannel;
    [SerializeField] VoidEventChannel hidePointerWindowChannel;
    [SerializeField] StructureEventChannel destroyHoverEnterChannel;
    [SerializeField] StructureEventChannel destroyHoverExitChannel;
    [SerializeField] StructureEventChannel destroyClickChannel;
    [SerializeField] UnityEvent onDestroy;
    [SerializeField] UnityEvent onError;

    bool active;

    private void OnEnable() {
        toggleDestroyPointerEventChannel.OnEventRaised += Toggle;
        destroyHoverEnterChannel.OnEventRaised += HoverEnter;
        destroyHoverExitChannel.OnEventRaised += HoverExit;
        destroyClickChannel.OnEventRaised += Click;
    }

    private void OnDisable() {
        toggleDestroyPointerEventChannel.OnEventRaised -= Toggle;
        destroyHoverEnterChannel.OnEventRaised -= HoverEnter;
        destroyHoverExitChannel.OnEventRaised -= HoverExit;
        destroyClickChannel.OnEventRaised -= Click;
    }

    public void HoverEnter(Structure structure) {
        string tooltip = "Right Click to Cancel";
        if (structure.destructible)
            tooltip += "\n<color=yellow>Click to destroy</color>";
        else
            tooltip += "\n<color=red>Can't destroy " + structure.name + "</color>";
        showPointerWindowChannel.Raise(tooltip);
    }

    public void HoverExit(Structure structure) {
        showPointerWindowChannel.Raise("Right Click to Cancel");
    }

    public void Click(Structure structure) {
        if(structure.destructible) {
            structure.Demolish();
            onDestroy?.Invoke();
        } else {
            onError?.Invoke();
        }
    }

    public void Toggle(ButtonAction action, ButtonState state) {
        if (state == ButtonState.Active) {
            Activate();
        }
        else {
            Deactivate();
        }
    }

    public void Activate() {
        active = true;
        Cursor.SetCursor(destroyCursorTexture, Vector2.zero, CursorMode.Auto);
        destroyPointer.Raise();
        showPointerWindowChannel.Raise("Right Click to Cancel");
    }

    public void Deactivate() {
        if(active) {
            active = false;
            Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
            defaultPointer.Raise();
            hidePointerWindowChannel.Raise();
        }
    }
}
