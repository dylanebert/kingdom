using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Terrain;
using Structures;

namespace Pointers
{
    public class PointerManager : MonoBehaviour
    {
        [SerializeField] CellRuntimeSet cells;
        [SerializeField] Pointer[] pointers;
        [SerializeField] IntVariable maxDragSize;

        Pointer activePointer;
        Cell prevHoverCell;
        Cell hoverCell;
        Cell dragStartCell;
        Structure prevHoverStructure;
        Structure hoverStructure;
        bool dragging;

        private void OnEnable() {
            for (int i = 0; i < pointers.Length; i++)
                pointers[i].OnEventRaised += SetPointer;
            activePointer = pointers[0];
            activePointer.onActivate?.Invoke();
        }

        private void OnDisable() {
            for (int i = 0; i < pointers.Length; i++)
                pointers[i].OnEventRaised -= SetPointer;
        }

        private void Update() {
            UpdateActivePointer();

            if (Input.GetKeyDown(KeyCode.Space))
                activePointer.onSpaceBar?.Invoke();
        }

        void UpdateActivePointer() {
            if (!cells.initialized) return;

            if (!activePointer.ignoreDrag && Input.GetMouseButtonUp(0) && dragging) {
                OnDragEnd(dragStartCell, hoverCell);
                dragging = false;
            }

            if (EventSystem.current.IsPointerOverGameObject())
                return;

            if (Input.GetKeyDown(KeyCode.Escape) || Input.GetMouseButtonDown(1)) {
                activePointer.onEscape?.Invoke();
                return;
            }

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            Plane ground = new Plane(Vector3.up, Vector3.zero);
            if (ground.Raycast(ray, out float dist)) {
                Vector3 hitPos = ray.GetPoint(dist);
                hoverCell = cells.WorldPosToCell(hitPos);
                if (dragging)
                    hoverCell = ClampEndTile(dragStartCell, hoverCell);
            }

            if (!activePointer.ignoreStructures && !dragging && Physics.Raycast(ray, out RaycastHit hit, float.PositiveInfinity, LayerMask.GetMask("Structures"))) {
                StructureInstance instance = hit.transform.GetComponent<StructureInstance>();
                hoverStructure = instance.structure;
            }
            else {
                hoverStructure = null;
            }

            if (Input.GetMouseButtonDown(0)) {
                dragStartCell = hoverCell;
                if (!activePointer.ignoreStructures && hoverStructure != null)
                    OnStructureClick(hoverStructure);
                else {
                    OnClick(hoverCell);
                }
            }

            dragging = !activePointer.ignoreDrag && Input.GetMouseButton(0);

            if (hoverCell != prevHoverCell) {
                if (activePointer.ignoreStructures || hoverStructure == null) {
                    if (dragging)
                        OnDrag(dragStartCell, hoverCell);
                    else {
                        if (prevHoverCell != null)
                            OnHoverExit(prevHoverCell);
                        OnHoverEnter(hoverCell);
                    }
                }
                prevHoverCell = hoverCell;
            }
            if(!activePointer.ignoreStructures && hoverStructure != prevHoverStructure) {
                if (prevHoverStructure != null)
                    OnStructureHoverExit(prevHoverStructure);
                if (hoverStructure != null)
                    OnStructureHoverEnter(hoverStructure);
                prevHoverStructure = hoverStructure;
            }
        }

        public void SetPointer(Pointer pointer) {
            if(pointer != activePointer) {
                Pointer prev = activePointer;
                activePointer = pointer;
                prev.onDeactivate?.Invoke();
                activePointer.onActivate?.Invoke();
                if (hoverCell != null)
                    OnHoverEnter(hoverCell);
            }
        }

        void OnHoverEnter(Cell cell) {
            activePointer.onHoverEnter?.Invoke(cell);
        }

        void OnHoverExit(Cell cell) {
            activePointer.onHoverExit?.Invoke(cell);
        }

        void OnClick(Cell cell) {
            activePointer.onClick?.Invoke(cell);
        }

        void OnDrag(Cell start, Cell end) {
            activePointer.onDrag?.Invoke(start, end);
        }

        void OnDragEnd(Cell start, Cell end) {
            activePointer.onDragEnd?.Invoke(start, end);
        }

        void OnStructureHoverEnter(Structure structure) {
            activePointer.onStructureHoverEnter?.Invoke(structure);
        }

        void OnStructureHoverExit(Structure structure) {
            activePointer.onStructureHoverExit?.Invoke(structure);
        }

        void OnStructureClick(Structure structure) {
            activePointer.onStructureClick.Invoke(structure);
        }

        public void Clear() {
            SetPointer(pointers[0]);
        }

        Cell ClampEndTile(Cell start, Cell end) {
            int x = end.x;
            int y = end.y;
            x = Mathf.Clamp(x, start.x - maxDragSize.Value, start.x + maxDragSize.Value);
            y = Mathf.Clamp(y, start.y - maxDragSize.Value, start.y + maxDragSize.Value);
            return cells.ClampedCell(x, y);
        }
    }
}