using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Events;

namespace Pointers
{
    [CreateAssetMenu(fileName = "Pointer", menuName = "Scriptable Objects/Pointers/Pointer")]
    public class Pointer : ScriptableObject
    {
        public UnityAction<Pointer> OnEventRaised;

        [Header("Activation")]
        public UnityEvent onActivate;
        public UnityEvent onDeactivate;

        [Header("Hover")]
        public CellEvent onHoverEnter;
        public CellEvent onHoverExit;
        public CellEvent onClick;

        [Header("Dragging")]
        public bool ignoreDrag;
        public CellDragEvent onDrag;
        public CellDragEvent onDragEnd;

        [Header("Structures")]
        public bool ignoreStructures;
        public StructureEvent onStructureHoverEnter;
        public StructureEvent onStructureHoverExit;
        public StructureEvent onStructureClick;

        [Header("Input keys")]
        public UnityEvent onSpaceBar;
        public UnityEvent onEscape;

        public void Raise() {
            OnEventRaised?.Invoke(this);
        }
    }
}