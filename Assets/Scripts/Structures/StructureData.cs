using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Terrain;
using Events;

namespace Structures
{
    [System.Serializable]
    public struct ResourceCost
    {
        public string key;
        public int amount;
    }

    [CreateAssetMenu(fileName = "StructureData", menuName = "Scriptable Objects/Structures/Structure Data")]
    public class StructureData : ScriptableObject 
    {
        [Space]
        [Header("Spatial data")]
        public Coords[] coords = new Coords[1];
        public bool hasEntrance;
        public Coords entrance;
        public bool walkable = true;
        public bool navigable = true;

        [Space]
        [Header("Construction data")]
        public bool requiresConstruction;
        public ResourceCost[] cost;
        public ConstructionSiteInstance constructionInstance;
        public SpatialEvent onBuilt;
        public float buildTime = 30;

        [Space]
        [Header("Destructibility")]
        public bool destructible = true;

        public bool IsValidPlacement(CellRuntimeSet cells, Cell pos, Quaternion rot, out string invalidPlacementReason) {
            invalidPlacementReason = "";
            for(int i = 0; i < coords.Length; i++) {
                Cell cell = cells.CoordsToCell(coords[i], pos, rot);
                if(cell.isWater) {
                    invalidPlacementReason = "Can't be placed on water";
                    return false;
                }
                if(cell.occupied) {
                    invalidPlacementReason = "Space is occupied";
                    return false;
                }
                if(!navigable && cell.reserved > 0) {
                    invalidPlacementReason = "Blocks entrance";
                    return false;
                }
            }
            if(hasEntrance) {
                Cell cell = cells.CoordsToCell(entrance, pos, rot);
                if(cell.isWater || !cell.walkable || !cell.navigable) {
                    invalidPlacementReason = "Entrance is blocked";
                    return false;
                }
            }
            return true;
        }
    }
}