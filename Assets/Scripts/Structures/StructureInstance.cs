using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Terrain;

namespace Structures
{
    public abstract class StructureInstance : MonoBehaviour
    {
        public StructureData data;

        public event UnityAction OnInitializeRaised;
        public event UnityAction OnDemolishRaised;

        [HideInInspector] public Structure structure;

        protected bool initialized;

        public abstract void OnInitialize();
        public abstract void OnDemolish();

        public virtual void Initialize(CellRuntimeSet cells, Cell pos, Quaternion rot, Transform parent) {
            transform.position = pos.WorldPos;
            transform.rotation = rot;
            transform.parent = parent;
            CreateStructure(cells, pos, rot);
            OnInitialize();
            OnInitializeRaised?.Invoke();
            structure.OnDemolish += Demolish;
            initialized = true;
        }

        public abstract void CreateStructure(CellRuntimeSet cells, Cell pos, Quaternion rot);

        public virtual void Demolish(Structure structure) {
            OnDemolish();
            OnDemolishRaised?.Invoke();
            initialized = false;
            Destroy(gameObject);
        }
    }
}