using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jobs;
using Skills;
using Animation;
using Terrain;

namespace Structures 
{
    [RequireComponent(typeof(FarmAnimator))]
    public class FarmInstance : JobComponent
    {
        [SerializeField] Skill skill;

        FarmAnimator animator;
        int phase;
        float growth;

        void Awake() {
            animator = GetComponent<FarmAnimator>();
        }

        public override void CreateStructure(CellRuntimeSet cells, Cell pos, Quaternion rot) {
            structure = Structure.Create(data, cells, pos, rot);
        }

        public override void ExecuteJob(Worker worker, Job job) {
            growth += skill.output;
            switch(phase) {
                case 0:
                    if(growth >= .1f) {
                        phase = 1;
                        Refresh();
                        animator.SetPhase(phase);
                    }
                    break;
                case 1:
                    if(growth >= .6f) {
                        phase = 2;
                        Refresh();
                        animator.SetPhase(phase);
                    }
                    break;
                case 2:
                    if(growth >= 1f) {
                        phase = 0;
                        skill.Contribute();
                        animator.SetPhase(0);
                        growth -= 1f;
                    }
                    break;
            }
        }

        void Refresh() {
            job.Terminate();
            jobRuntimeSet.Register(job);
        }
    }
}
