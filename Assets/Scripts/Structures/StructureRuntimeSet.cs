using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Structures 
{
    [CreateAssetMenu(fileName = "StructureRuntimeSet", menuName = "Scriptable Objects/Structures/Structure Runtime Set")]
    [Serializable]
    public class StructureRuntimeSet : ScriptableObject
    {
        public List<Structure> structures;
        [NonSerialized] public bool initialized;

        private void OnEnable() {
            initialized = false;
        }

        public void Initialize(List<Structure> structures) {
            this.structures = structures;
            initialized = true;
        }

        public void Initialize(string json) {
            JsonUtility.FromJsonOverwrite(json, this);
            initialized = true;
        }

        public void Register(Structure structure) {
            if (!structures.Contains(structure))
                structures.Add(structure);
            structure.OnDemolish += Unregister;
        }

        public void Unregister(Structure structure) {
            structure.OnDemolish -= Unregister;
            if (structures.Contains(structure))
                structures.Remove(structure);
        }
    }
}