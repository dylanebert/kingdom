using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Terrain;
using Events;

namespace Structures
{
    [System.Serializable]
    public class ConstructionSite : Structure
    {
        public ResourceCost[] costs;
        public SpatialEvent onBuilt;
        public float buildTime;
        public float progress;

        public static new ConstructionSite Create(StructureData data, CellRuntimeSet cells, Cell pos, Quaternion rot) {
            ConstructionSite site = CreateInstance<ConstructionSite>();
            site.ApplyData(data, cells, pos, rot);
            return site;
        }

        public override void ApplyData(StructureData data, CellRuntimeSet cells, Cell pos, Quaternion rot) {
            costs = data.cost;
            onBuilt = data.onBuilt;
            buildTime = data.buildTime;
            progress = 0;
            base.ApplyData(data, cells, pos, rot);
        }
    }
}