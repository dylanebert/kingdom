using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Events;
using Terrain;

namespace Structures 
{
    public class KeepInstance : StructureInstance
    {
        [SerializeField] CellEventChannel createCitizenChannel;
        [SerializeField] IntVariable initialCitizenCount;

        public override void CreateStructure(CellRuntimeSet cells, Cell pos, Quaternion rot) {
            structure = Structure.Create(data, cells, pos, rot);
        }

        public override void OnInitialize() {
            for (int i = 0; i < initialCitizenCount.Value; i++)
                createCitizenChannel.Raise(structure.entrance);
        }

        public override void OnDemolish() { }
    }
}