using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Terrain;
using Events;

namespace Structures
{
    [System.Serializable]
    public class Tree : Structure
    {
        public int yield;
        public float health;
        public bool selected;
        public SpatialEvent onHit;
        public SpatialEvent onHitTop;
        public SpatialEvent onFall;
        public FloatEvent onFirstHitGround;
        public SpatialEvent onHitGround;
        public SpatialEvent onPop;
        
        public event UnityAction OnSelect;

        public static Tree Create(TreeData data, CellRuntimeSet cells, Cell pos, Quaternion rot) {
            Tree tree = CreateInstance<Tree>();
            tree.ApplyData(data, cells, pos, rot);
            return tree;
        }

        public override void ApplyData(StructureData data, CellRuntimeSet cells, Cell pos, Quaternion rot) {
            TreeData treeData = data as TreeData;
            yield = treeData.yield;
            health = treeData.maxHealth;
            onHit = treeData.onHit;
            onHitTop = treeData.onHitTop;
            onFall = treeData.onFall;
            onFirstHitGround = treeData.onFirstHitGround;
            onHitGround = treeData.onHitGround;
            onPop = treeData.onPop;
            base.ApplyData(data, cells, pos, rot);            
        }

        public void Select() {
            OnSelect?.Invoke();
        }
    }
}