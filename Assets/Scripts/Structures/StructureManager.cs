using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Events;
using Procedural;
using Terrain;

namespace Structures
{
    public class StructureManager : MonoBehaviour
    {
        [SerializeField] StructureInstanceGroup[] structureInstanceGroups;
        [SerializeField] StructureInstanceEventChannel blueprintPlacementChannel;
        [SerializeField] GenerationEventChannel generationEventChannel;
        [SerializeField] CellRuntimeSet cells;
        [SerializeField] StructureRuntimeSet structures;

        Dictionary<string, StructureInstanceGroup> instanceDict = new Dictionary<string, StructureInstanceGroup>();

        void Awake() {
            for(int i = 0; i < structureInstanceGroups.Length; i++)
                instanceDict.Add(structureInstanceGroups[i].name, structureInstanceGroups[i]);
        }

        void OnEnable() {
            blueprintPlacementChannel.OnEventRaised += Place;
            generationEventChannel.OnEventRaised += Create;
        }

        void OnDisable() {
            blueprintPlacementChannel.OnEventRaised -= Place;
            generationEventChannel.OnEventRaised += Create;
        }

        public void Generate(ProceduralData proceduralData) {
            Cell pos;
            StructureInstance instance;
            Quaternion rot;
            for(int y = 0; y < cells.cells.GetLength(1); y++) {
                for(int x = 0; x < cells.cells.GetLength(0); x++) {
                    pos = cells.cells[x, y];
                    if(pos.generationTag != null) {
                        instance = Instantiate(instanceDict[pos.generationTag].Next());
                        rot = Quaternion.Euler(0, Random.Range(0, 360f), 0);
                        instance.Initialize(cells, pos, rot, transform);
                    }
                }
            }
        }

        public void Save() {
            SaveSystem.SaveStructures(structures);
        }

        public void Load() {
            SaveSystem.LoadStructures(structures);
        }

        public void Create(string key, Cell pos, Quaternion rot) {
            StructureInstance instance = Instantiate(instanceDict[key].Next());
            instance.Initialize(cells, pos, rot, transform);
        }

        public void Place(StructureInstance instance, Cell pos, Quaternion rot) {
            if(instance.data.requiresConstruction) {
                ConstructionSiteInstance site = Instantiate(instance.data.constructionInstance);
                site.data = instance.data;
                site.Initialize(cells, pos, rot, transform);
            } else {
                StructureInstance structureInstance = Instantiate(instance);
                structureInstance.Initialize(cells, pos, rot, transform);
            }            
        }

        public void OnStructureDemolished(Structure structure) {
            structure.OnDemolish -= OnStructureDemolished;
            structures.Unregister(structure);
        }
    }
}