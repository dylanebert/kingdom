using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Events;
using Pointers;
using Terrain;

namespace Structures
{
    public class TreeSelectionManager : MonoBehaviour
    {
        [SerializeField] CellRuntimeSet cells;
        [SerializeField] ButtonEventChannel toggleTreeSelectionChannel;
        [SerializeField] Pointer defaultPointer;
        [SerializeField] Pointer chopPointer;
        [SerializeField] StringEventChannel showPointerWindowChannel;
        [SerializeField] VoidEventChannel hidePointerWindowChannel;
        [SerializeField] CellDragEventChannel dragEndChannel;
        [SerializeField] VoidEventChannel showChopCancelButtonChannel;
        [SerializeField] AudioSource audioSource;

        bool active;

        private void OnEnable() {
            toggleTreeSelectionChannel.OnEventRaised += Toggle;
            dragEndChannel.OnEventRaised += OnDragEnd;
        }

        private void OnDisable() {
            toggleTreeSelectionChannel.OnEventRaised -= Toggle;
            dragEndChannel.OnEventRaised -= OnDragEnd;
        }

        public void Toggle(ButtonAction action, ButtonState state) {
            if (state == ButtonState.Active)
                Activate();
            else
                Deactivate();
        }

        public void Activate() {
            active = true;
            chopPointer.Raise();
            showPointerWindowChannel.Raise("Drag to select trees\nRight Click to Cancel");
        }

        public void Deactivate() {
            if (active) {
                active = false;
                defaultPointer.Raise();
                hidePointerWindowChannel.Raise();
            }
        }

        public void OnDragEnd(Cell start, Cell end) {
            int signX = (int)Mathf.Sign(end.x - start.x);
            int signY = (int)Mathf.Sign(end.y - start.y);
            int row = Mathf.Abs(end.x - start.x) + 1;
            int col = Mathf.Abs(end.y - start.y) + 1;
            List<Tree> selected = new List<Tree>();
            for (int line = 1; line <= (row + col - 1); line++) {
                int startCol = Mathf.Max(0, line - row);
                int count = Mathf.Min(line, Mathf.Min((col - startCol), row));
                for (int j = 0; j < count; j++) {
                    int x = Mathf.Min(row, line) - j - 1;
                    int y = startCol + j;
                    Cell cell = cells.ClampedCell(start.x + signX * x, start.y + signY * y);
                    if (cell.structure != null && cell.structure is Tree && !((Tree)cell.structure).selected) {
                        selected.Add((Tree)cell.structure);
                    }
                }
            }
            if (selected.Count > 0) {
                StartCoroutine(SelectTreesCoroutine(selected));
                showChopCancelButtonChannel.Raise();
            }
        }

        IEnumerator SelectTreesCoroutine(List<Tree> selected) {
            int soundCount = Mathf.Min(selected.Count, 4);
            int interval = Mathf.Max(1, Mathf.RoundToInt(selected.Count / (float)soundCount));
            audioSource.pitch = 1f;
            for (int i = 0; i < selected.Count; i++) {
                if (i % interval == 0) {
                    audioSource.pitch += Random.Range(.05f, .15f);
                    audioSource.Play();
                    yield return new WaitForSecondsRealtime(.07f);
                }
                selected[i].Select();
            }
        }
    }
}