using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Structures
{
    [CreateAssetMenu(fileName = "StructureInstanceGroup", menuName = "Scriptable Objects/Structures/Structure Instance Group")]
    public class StructureInstanceGroup : ScriptableObject
    {
        public StructureInstance[] instances;

        int next = -1;
        int prev = -1;

        public StructureInstance Next() {
            if (instances.Length == 1)
                return instances[0];

            if (next == -1) {
                next = Random.Range(0, instances.Length);
            }
            else {
                do {
                    next = Random.Range(0, instances.Length);
                } while (next == prev);
            }
            prev = next;
            return instances[next];
        }
    }
}