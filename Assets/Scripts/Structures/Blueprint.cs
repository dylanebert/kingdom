using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Structures;
using Events;
using Pointers;
using Terrain;

public class Blueprint : MonoBehaviour
{
    [SerializeField] GameObject contents;
    [SerializeField] CellRuntimeSet cells;
    [SerializeField] Pointer defaultPointer;
    [SerializeField] StructureInstanceEventChannel blueprintPlacementChannel;
    [SerializeField] BlueprintDataEventChannel blueprintCreationChannel;
    [SerializeField] VoidEventChannel destroyBlueprintChannel;
    [SerializeField] CellEventChannel blueprintHoverChannel;
    [SerializeField] CellEventChannel blueprintClickChannel;
    [SerializeField] VoidEventChannel onSpaceBarChannel;
    [SerializeField] StringEventChannel showPointerWindowChannel;
    [SerializeField] VoidEventChannel hidePointerWindowChannel;
    [SerializeField] UnityEvent onPlacementError;

    Cell prevCell;
    Cell targetCell;
    Quaternion prevRot;
    Quaternion targetRot;
    BlueprintData data;
    StructureInstance instance;
    float tPos, vPos, tRot, vRot, shake;

    private void OnEnable() {
        blueprintCreationChannel.OnEventRaised += CreateBlueprint;
        destroyBlueprintChannel.OnEventRaised += DestroyBlueprint;
        blueprintHoverChannel.OnEventRaised += OnPointerHover;
        blueprintClickChannel.OnEventRaised += OnPointerClick;
        onSpaceBarChannel.OnEventRaised += Rotate;
    }

    private void OnDisable() {
        blueprintCreationChannel.OnEventRaised -= CreateBlueprint;
        destroyBlueprintChannel.OnEventRaised -= DestroyBlueprint;
        blueprintHoverChannel.OnEventRaised -= OnPointerHover;
        blueprintClickChannel.OnEventRaised -= OnPointerClick;
        onSpaceBarChannel.OnEventRaised -= Rotate;
    }

    private void Update() {
        if(instance != null) {
            if(prevCell != null && targetCell != null) {
                if(tPos < 1) {
                    tPos += Time.unscaledDeltaTime * 4;
                    vPos = Easing.OutBack(tPos);
                    contents.transform.localPosition = Vector3.Lerp(prevCell.WorldPos, targetCell.WorldPos, vPos);
                }
                UpdateValidPlacement();
            }
            if(tRot < 1) {
                tRot += Time.unscaledDeltaTime * 4;
                vRot = Easing.OutBack(tRot);
                instance.transform.rotation = Quaternion.Slerp(prevRot, targetRot, vRot);
            }
            if(shake > 0) {
                shake -= Time.unscaledDeltaTime;
                instance.transform.localPosition = Random.insideUnitSphere * shake * shake * Mathf.Max(0, shake) * 5;
            }

            UpdateValidPlacement();
        }
    }

    bool UpdateValidPlacement() {
        if (instance == null || targetCell == null) return false;
        string tooltip = data.placementTooltip;
        bool valid = instance.data.IsValidPlacement(cells, targetCell, targetRot, out string invalidPlacementReason);
        if(valid) {
            tooltip += "\n<color=green>Can be placed here</color>";
        } else {
            tooltip += "\n<color=red>" + invalidPlacementReason + "</color>";
        }
        showPointerWindowChannel.Raise(tooltip);
        return valid;
    }

    public void CreateBlueprint(BlueprintData data) {
        showPointerWindowChannel.Raise(data.placementTooltip);
        if (instance != null)
            Destroy(instance.gameObject);
        this.data = data;
        instance = Instantiate(data.instanceGroup.Next());
        instance.transform.parent = contents.transform;
        instance.transform.localPosition = Vector3.zero;
        instance.transform.rotation = prevRot = targetRot = data.cachedRotation;
    }

    public void DestroyBlueprint() {
        hidePointerWindowChannel.Raise();
        if(instance != null) {
            Destroy(instance.gameObject);
            instance = null;
        }
    }

    public void OnPointerHover(Cell cell) {
        if (prevCell == null)
            prevCell = cell;
        prevCell = targetCell;
        targetCell = cell;
        tPos = 0;
    }

    public void OnPointerClick(Cell cell) {
        if(UpdateValidPlacement()) {
            if(data.repeatable) {
                StructureInstance repeat = Repeat();
                Place();
                instance = repeat;
            } else {
                Place();
                defaultPointer.Raise();
            }
        } else {
            onPlacementError?.Invoke();
            shake = .5f;
        }        
    }

    public void Place() {
        data.onPlace?.Invoke(targetCell);
        blueprintPlacementChannel.Raise(instance, targetCell, targetRot);
        Destroy(instance.gameObject);
        instance = null;
    }

    public StructureInstance Repeat() {
        StructureInstance repeat = Instantiate(instance);
        repeat.transform.parent = contents.transform;
        repeat.transform.localPosition = Vector3.zero;
        repeat.transform.rotation = targetRot;
        return repeat;
    }

    public void Rotate() {
        if (instance == null) return;
        prevRot = instance.transform.rotation;
        targetRot *= Quaternion.Euler(0, 90, 0);
        data.cachedRotation = targetRot;
        tRot = 0;
    }
}
