using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Terrain;
using Pathfinding;
using System;

namespace Structures
{
    [Serializable]
    public class Structure : ScriptableObject
    {
        public Coords[] coords;
        public bool hasEntrance;
        public Coords entranceCoords;
        public bool walkable;
        public bool navigable;
        public bool destructible;
        public int x, y;
        public Quaternion rot;

        public event UnityAction<Structure> OnDemolish;

        [NonSerialized] public Cell[] cells;
        [NonSerialized] public Cell entrance;

        public static Structure Create(StructureData data, CellRuntimeSet cells, Cell pos, Quaternion rot) {
            Structure structure = CreateInstance<Structure>();
            structure.ApplyData(data, cells, pos, rot);
            return structure;
        }

        public virtual void ApplyData(StructureData data, CellRuntimeSet cells, Cell pos, Quaternion rot) {
            x = pos.x;
            y = pos.y;
            this.rot = rot;
            name = data.name;
            coords = data.coords;
            hasEntrance = data.hasEntrance;
            entranceCoords = data.entrance;
            walkable = data.walkable;
            navigable = data.navigable;
            destructible = data.destructible;
            Initialize(cells);    
        }

        public void Initialize(CellRuntimeSet cells) {
            Cell pos = cells.cells[x, y];
            this.cells = new Cell[coords.Length];
            for (int i = 0; i < coords.Length; i++) {
                this.cells[i] = cells.CoordsToCell(coords[i], pos, rot);
                this.cells[i].walkable = walkable;
                this.cells[i].navigable = navigable;
                this.cells[i].occupied = true;
                this.cells[i].structure = this;
            }

            if (hasEntrance) {
                entrance = cells.CoordsToCell(entranceCoords, pos, rot);
                entrance.reserved++;
            }

            UpdatePathfinding();
        }

        public void Demolish() {
            for (int i = 0; i < cells.Length; i++) {
                if(cells[i].structure == this) {
                    cells[i].walkable = true;
                    cells[i].navigable = true;
                    cells[i].occupied = false;
                }                
            }
            if (entrance != null)
                entrance.reserved--;

            UpdatePathfinding();
            OnDemolish?.Invoke(this);
        }

        void UpdatePathfinding() {
            int[] invert = new int[4] { 2, 3, 0, 1 };
            AstarPath.active.AddWorkItem(new AstarWorkItem(ctx => {
                GridGraph gg = AstarData.active.data.gridGraph;
                for (int i = 0; i < cells.Length; i++) {
                    Cell cell = cells[i];
                    GridNode node = cell.Node;
                    node.Walkable = cell.walkable;
                    node.Tag = cell.PathfindingTag;

                    GridNode[] adj = new GridNode[4] {
                        gg.GetNode(cell.x, cell.y - 1) as GridNode,
                        gg.GetNode(cell.x + 1, cell.y) as GridNode,
                        gg.GetNode(cell.x, cell.y + 1) as GridNode,
                        gg.GetNode(cell.x - 1, cell.y) as GridNode,
                    };
                    
                    if(node.Tag < 2) {
                        for (int j = 0; j < 4; j++) {
                            node.SetConnectionInternal(j, false);
                            adj[j].SetConnectionInternal(invert[j], false);
                        }
                    } else if(node.Tag == 2) {
                        for(int j = 0; j < 4; j++) {
                            GridNode adjNode = adj[j];
                            bool connected = false;
                            for (int k = 0; k < cells.Length; k++)
                                if (cells[k].Node == adjNode)
                                    connected = true;
                            if (hasEntrance && entrance.Node == adjNode)
                                connected = true;
                            if (cell.reserved > 0 && !connected) continue;
                            node.SetConnectionInternal(j, connected);
                            adjNode.SetConnectionInternal(invert[j], connected);
                        }
                    } else {
                        for(int j = 0; j < 4; j++) {
                            GridNode adjNode = adj[j];
                            bool connected = adjNode.Tag > 2;
                            if (cell.reserved > 0 && !connected) continue;
                            node.SetConnectionInternal(j, adjNode.Tag > 2);
                            adjNode.SetConnectionInternal(invert[j], adjNode.Tag > 2);
                        }
                    }
                }
            }));
        }
    }
}