using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jobs;

namespace Structures 
{
    public abstract class JobComponent : StructureInstance
    {
        [SerializeField] protected JobRuntimeSet jobRuntimeSet;
        [SerializeField] protected int maxWorkers = 1;

        protected Job job;
        
        public override void OnInitialize() {
            job = new Job(gameObject, maxWorkers);
            jobRuntimeSet.Register(job);
            job.onExecute += ExecuteJob;
        }

        public override void OnDemolish() {
            job.onExecute -= ExecuteJob;
            job.Terminate();
        }

        public abstract void ExecuteJob(Worker worker, Job job);
    }
}