using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Terrain;

namespace Structures 
{
    public class SceneryInstance : StructureInstance
    {
        public override void CreateStructure(CellRuntimeSet cells, Cell pos, Quaternion rot) {
            structure = Structure.Create(data, cells, pos, rot);
        }

        public override void OnInitialize() { }
        public override void OnDemolish() { }
    }
}

