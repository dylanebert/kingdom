using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Events;
using Logistics;
using Jobs;
using Skills;
using Terrain;

namespace Structures 
{
    public class ConstructionSiteInstance : JobComponent
    {
        [SerializeField] GenerationEventChannel generationEventChannel;
        [SerializeField] Skill skill;
        [SerializeField] SpatialEvent onDeposit;
        [SerializeField] SpatialEvent onTickConstruction;
        [SerializeField] PromiseDataEventChannel promiseDataEventChannel;
        [SerializeField] ProgressIcon progressIcon;

        List<Destination> consumers = new List<Destination>();
        ConstructionSite constructionSite;
        int phase = 0;

        public override void CreateStructure(CellRuntimeSet cells, Cell pos, Quaternion rot) {
            structure = constructionSite = ConstructionSite.Create(data, cells, pos, rot);
        }

        public override void OnInitialize() {
            job = new Job(gameObject, maxWorkers);
            for(int i = 0; i < constructionSite.costs.Length; i++) {
                Destination consumer = new Destination(gameObject, constructionSite.costs[i].key, 0, constructionSite.costs[i].amount);
                consumer.OnDepositRaised += OnDeposit;
                consumers.Add(consumer);
            }
        }

        public override void OnDemolish() {
            for (int i = 0; i < consumers.Count; i++) {
                consumers[i].OnDepositRaised -= OnDeposit;
                consumers[i].Release();
            }
            job.onExecute -= ExecuteJob;
            job.Terminate();
        }

        private void Update() {
            if (phase > 0) return;
            phase = 1;
            for (int i = 0; i < consumers.Count; i++) {
                if (consumers[i].Count < consumers[i].Capacity) {
                    phase = 0;
                    if (consumers[i].HasSpace)
                        promiseDataEventChannel.Raise(null, consumers[i], null);
                }
            }
            if(phase == 1) {
                progressIcon.Show();
                jobRuntimeSet.Register(job);
                job.onExecute += ExecuteJob;
            }
        }

        public void OnDeposit() {
            onDeposit?.Invoke(transform.position);
        }

        public override void ExecuteJob(Worker worker, Job job) {
            onTickConstruction?.Invoke(transform.position);
            float progress = skill.output / constructionSite.buildTime;
            constructionSite.progress += progress;
            progressIcon.SetProgress(constructionSite.progress);
            if(constructionSite.progress >= 1) {
                skill.Contribute();
                generationEventChannel.Raise(constructionSite.name, constructionSite.cells[0], constructionSite.rot);
                constructionSite.onBuilt?.Invoke(transform.position);
                constructionSite.Demolish();
            }
        }
    }
}