using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Structures;
using Events;
using UI;
using UnityEngine.Events;

namespace Structures
{
    [CreateAssetMenu(fileName = "BlueprintData", menuName = "Scriptable Objects/Structures/Blueprint Data")]
    public class BlueprintData : ScriptableObject
    {
        public StructureInstanceGroup instanceGroup;
        public CellEvent onPlace;
        public string placementTooltip = "Space to Rotate\nRight Click to Cancel";
        public bool unlocked = true;
        public bool repeatable = true;

        [HideInInspector] public Quaternion cachedRotation = Quaternion.identity;

        private void OnEnable() {
            cachedRotation = Quaternion.identity;
        }
    }
}