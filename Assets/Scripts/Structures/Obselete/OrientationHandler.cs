using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Structures;

public class OrientationHandler : MonoBehaviour
{
    /// <summary>
    /// Dictionary to look up which piece to use, depending on adjacent tiles
    /// </summary>
    static Dictionary<int[], Orientation> Lookup = new Dictionary<int[], Orientation>(new LookupComparer()) {
        //North, South, East, West
        { new int[] { 0, 0, 0, 0 }, Orientation.None },
        { new int[] { 0, 0, 1, 0 }, Orientation.Horizontal },
        { new int[] { 0, 0, 0, 1 }, Orientation.Horizontal },
        { new int[] { 0, 0, 1, 1 }, Orientation.Horizontal },
        { new int[] { 1, 0, 0, 0 }, Orientation.Vertical },
        { new int[] { 0, 1, 0, 0 }, Orientation.Vertical },
        { new int[] { 1, 1, 0, 0 }, Orientation.Vertical },
        { new int[] { 1, 0, 0, 1 }, Orientation.NW },
        { new int[] { 1, 0, 1, 0 }, Orientation.NE },
        { new int[] { 0, 1, 1, 0 }, Orientation.SE },
        { new int[] { 0, 1, 0, 1 }, Orientation.SW },
        { new int[] { 1, 0, 1, 1 }, Orientation.TriN },
        { new int[] { 0, 1, 1, 1 }, Orientation.TriS },
        { new int[] { 1, 1, 1, 0 }, Orientation.TriE },
        { new int[] { 1, 1, 0, 1 }, Orientation.TriW },
        { new int[] { 1, 1, 1, 1 }, Orientation.Cross }
    };

    public OrientationModels[] orientations = new OrientationModels[] {
        new OrientationModels() { orientation = Orientation.None },
        new OrientationModels() { orientation = Orientation.Horizontal },
        new OrientationModels() { orientation = Orientation.Vertical },
        new OrientationModels() { orientation = Orientation.NW },
        new OrientationModels() { orientation = Orientation.NE },
        new OrientationModels() { orientation = Orientation.SE },
        new OrientationModels() { orientation = Orientation.SW },
        new OrientationModels() { orientation = Orientation.TriN },
        new OrientationModels() { orientation = Orientation.TriS },
        new OrientationModels() { orientation = Orientation.TriE },
        new OrientationModels() { orientation = Orientation.TriW },
        new OrientationModels() { orientation = Orientation.Cross },
    };

    List<GameObject> models;
    Dictionary<Orientation, OrientationModels> orientationDict;
    Cell tile;
    Cell[] adjacent;

    /*void Awake() {
        models = new List<GameObject>();
        orientationDict = new Dictionary<Orientation, OrientationModels>();
        foreach (OrientationModels orientationModels in orientations) {
            orientationDict.Add(orientationModels.orientation, orientationModels);
            models.AddRange(orientationModels.models);
        }
    }

    private void Start() {
        Debug.Assert(structure.BuildingTiles.Count == 1);
        tile = structure.BuildingTiles[0];
        adjacent = tile.Adjacent;
        foreach (GameObject model in models)
            model.SetActive(false);
        UpdateAppearance();
        for (int i = 0; i < 4; i++) {
            Tile adj = adjacent[i];
            if (adj.Building != null && adj.Building.data.tag == structure.data.tag && adj.Building.GetComponent<OrientationHandler>() != null)
                adj.Building.GetComponent<OrientationHandler>().UpdateAppearance();
        }
    }

    public void UpdateAppearance() {        
        Tile tile = structure.BuildingTiles[0];
        int[] vec = new int[] { 0, 0, 0, 0 };
        for (int i = 0; i < 4; i++) {
            Tile adj = tile.Adjacent[i];
            if (adj.Building != null) {
                if (adj.Building.data.tag == structure.data.tag) {
                    vec[i] = 1;
                }
                if(structure.data.tag == "Path" && adj.Building.EntranceTile == tile) {
                    vec[i] = 1;
                }
            }
        }

        if (Lookup[vec] != Orientation.None)
            transform.rotation = Quaternion.identity;
        OrientationModels orientationModels = orientationDict[Lookup[vec]];
        if (!orientationModels.Active) {
            foreach (GameObject model in models)
                model.SetActive(false);
            orientationModels.SampleModel.SetActive(true);
        }
    }

    private void OnDisable() {
        if (!initialized || structure == null)
            return;
        foreach (Tile adj in structure.BuildingTiles[0].Adjacent) {
            if (adj.Building != null && adj.Building.data.tag == structure.data.tag && adj.Building.GetComponent<OrientationHandler>() != null)
                adj.Building.GetComponent<OrientationHandler>().UpdateAppearance();
        }
    }*/

    [System.Serializable]
    public class OrientationModels
    {
        public Orientation orientation;
        public GameObject[] models;
        public GameObject SampleModel => models[Random.Range(0, models.Length)];
        public bool Active {
            get {
                foreach (GameObject model in models)
                    if (model.activeSelf)
                        return true;
                return false;
            }
        }
    }

    public enum Orientation
    {
        None,
        Horizontal,
        Vertical,
        NW,
        NE,
        SW,
        SE,
        TriN,
        TriS,
        TriE,
        TriW,
        Cross
    }

    public class LookupComparer : IEqualityComparer<int[]>
    {
        public bool Equals(int[] x, int[] y) {
            if (x.Length != y.Length)
                return false;
            for (int i = 0; i < x.Length; i++) {
                if (x[i] != y[i])
                    return false;
            }
            return true;
        }

        public int GetHashCode(int[] obj) {
            int result = 17;
            for (int i = 0; i < obj.Length; i++) {
                unchecked {
                    result = result * 23 + obj[i];
                }
            }
            return result;
        }
    }
}
