using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Events;
using Terrain;

namespace Structures
{
    [CreateAssetMenu(fileName = "TreeData", menuName = "Scriptable Objects/Structures/Tree Data")]
    public class TreeData : StructureData
    {
        public int yield;
        public float maxHealth;
        public SpatialEvent onHit;
        public SpatialEvent onHitTop;
        public SpatialEvent onFall;
        public FloatEvent onFirstHitGround;
        public SpatialEvent onHitGround;
        public SpatialEvent onPop;
    } 
}