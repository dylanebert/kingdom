using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jobs;
using Events;
using Skills;
using Terrain;

namespace Structures
{
    public class TreeInstance : JobComponent
    {
        [SerializeField] Skill skill;
        [SerializeField] LooseResourceEventChannel looseResourceEventChannel;
        [SerializeField] Icon axeIcon;
        [SerializeField] VoidEventChannel chopCancelChannel;
        [SerializeField] CameraFocusVariable cameraFocusVariable;

        Tree tree;
        float height = 10;

        public override void CreateStructure(CellRuntimeSet cells, Cell pos, Quaternion rot) {
            structure = tree = Tree.Create((TreeData)data, cells, pos, rot);
        }

        public override void OnInitialize() {
            job = new Job(gameObject, maxWorkers);
            tree.OnSelect += OnSelect;
        }

        public void OnSelect() {
            if (tree.selected) return;
            axeIcon.Show();
            jobRuntimeSet.Register(job);
            job.onExecute += ExecuteJob;
            chopCancelChannel.OnEventRaised += OnDeselect;
            tree.selected = true;
        }

        public void OnDeselect() {
            if (!tree.selected) return;
            chopCancelChannel.OnEventRaised -= OnDeselect;
            job.onExecute -= ExecuteJob;
            axeIcon.Hide();
            job.Terminate();
            tree.selected = false;
        }

        public override void ExecuteJob(Worker worker, Job job) {
            if (tree.health <= 0)
                return;
            float damage = skill.output;
            StartCoroutine(HitCoroutine(damage));
        }

        IEnumerator HitCoroutine(float damage) {
            yield return new WaitForSeconds(.5f);
            tree.onHit?.Invoke(transform.position + Vector3.up);
            tree.onHitTop?.Invoke(transform.position + Vector3.up * height);
            StartCoroutine(ShakeCoroutine());
            tree.health -= damage;
            if (tree.health <= 0)
                StartCoroutine(FallCoroutine());
        }

        IEnumerator ShakeCoroutine() {
            float t = 0f;
            Vector3 pos = transform.position;
            Vector3 shake = transform.right;
            while (t < 1f) {
                t += Time.deltaTime;
                transform.position = pos + shake * Mathf.Sin(t * 30f) * .1f * (1 - t);
                yield return null;
            }
        }

        IEnumerator FallCoroutine() {
            skill.Contribute();
            axeIcon.Hide();
            tree.onFall.Invoke(transform.position);

            GameObject parent = new GameObject("Falling Tree");
            parent.transform.position = transform.position;
            parent.transform.rotation = transform.rotation;
            transform.SetParent(parent.transform);

            int i = 0;
            float[] thresholds = new float[] { .3636f, .7273f, .9091f };
            float t = 0f;
            float s = 0f;
            while (t < 1f) {
                t += Time.deltaTime / 6f;
                float c = 1 / 2.75f;
                float v = t * 3;
                bool bounce = false;
                if (v < 2f) {
                    v /= 2f;
                    v = Mathf.Pow(2, 10 * v - 10);
                }
                else {
                    v -= 2f;
                    v = (1 - c) * v + c;
                    s = v;
                    v = Easing.Bounce(v);
                    bounce = true;
                }
                float xRot = Mathf.Lerp(0f, 90f, v);
                transform.localRotation = Quaternion.Euler(xRot, 0, 0);
                if (bounce && i < thresholds.Length) {
                    if (s > thresholds[i]) {
                        if(i == 0) {
                            tree.onFirstHitGround?.Invoke(cameraFocusVariable.ComputeFocus(transform.position));
                        }
                        tree.onHitGround?.Invoke(transform.position + transform.up * height / 2f);
                        i++;
                    }
                }
                yield return null;
            }

            tree.onPop.Invoke(transform.position);
            looseResourceEventChannel.Raise(transform.position, "Wood", tree.yield);

            tree.Demolish();
        }
    }
}